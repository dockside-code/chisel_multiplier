module mult_loom_3(
  input         clock,
  input         reset,
  input  [15:0] io_activations_0,
  input  [15:0] io_activations_1,
  input  [15:0] io_activations_2,
  input  [15:0] io_activations_3,
  input  [15:0] io_activations_4,
  input  [15:0] io_activations_5,
  input  [15:0] io_activations_6,
  input  [15:0] io_activations_7,
  input  [15:0] io_activations_8,
  input  [15:0] io_activations_9,
  input  [15:0] io_activations_10,
  input  [15:0] io_activations_11,
  input  [15:0] io_activations_12,
  input  [15:0] io_activations_13,
  input  [15:0] io_activations_14,
  input  [15:0] io_activations_15,
  input  [15:0] io_weight_0,
  input  [15:0] io_weight_1,
  input  [15:0] io_weight_2,
  input  [15:0] io_weight_3,
  input  [15:0] io_weight_4,
  input  [15:0] io_weight_5,
  input  [15:0] io_weight_6,
  input  [15:0] io_weight_7,
  input  [15:0] io_weight_8,
  input  [15:0] io_weight_9,
  input  [15:0] io_weight_10,
  input  [15:0] io_weight_11,
  input  [15:0] io_weight_12,
  input  [15:0] io_weight_13,
  input  [15:0] io_weight_14,
  input  [15:0] io_weight_15,
  input  [7:0]  io_sw_length,
  input         io_reset,
  input         io_loadingValues,
  output [31:0] io_outputProduct
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [31:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [31:0] _RAND_10;
  reg [31:0] _RAND_11;
  reg [31:0] _RAND_12;
  reg [31:0] _RAND_13;
  reg [31:0] _RAND_14;
  reg [31:0] _RAND_15;
  reg [31:0] _RAND_16;
  reg [31:0] _RAND_17;
  reg [31:0] _RAND_18;
  reg [31:0] _RAND_19;
  reg [31:0] _RAND_20;
  reg [31:0] _RAND_21;
  reg [31:0] _RAND_22;
  reg [31:0] _RAND_23;
  reg [31:0] _RAND_24;
  reg [31:0] _RAND_25;
  reg [31:0] _RAND_26;
  reg [31:0] _RAND_27;
  reg [31:0] _RAND_28;
  reg [31:0] _RAND_29;
  reg [31:0] _RAND_30;
  reg [31:0] _RAND_31;
  reg [31:0] _RAND_32;
  reg [31:0] _RAND_33;
  reg [31:0] _RAND_34;
  reg [31:0] _RAND_35;
  reg [31:0] _RAND_36;
  reg [31:0] _RAND_37;
  reg [31:0] _RAND_38;
  reg [31:0] _RAND_39;
  reg [31:0] _RAND_40;
  reg [31:0] _RAND_41;
  reg [31:0] _RAND_42;
  reg [31:0] _RAND_43;
  reg [31:0] _RAND_44;
  reg [31:0] _RAND_45;
  reg [31:0] _RAND_46;
  reg [31:0] _RAND_47;
  reg [31:0] _RAND_48;
  reg [31:0] _RAND_49;
  reg [31:0] _RAND_50;
  reg [31:0] _RAND_51;
  reg [31:0] _RAND_52;
  reg [31:0] _RAND_53;
  reg [31:0] _RAND_54;
  reg [31:0] _RAND_55;
  reg [31:0] _RAND_56;
  reg [31:0] _RAND_57;
  reg [31:0] _RAND_58;
  reg [31:0] _RAND_59;
  reg [31:0] _RAND_60;
  reg [31:0] _RAND_61;
  reg [31:0] _RAND_62;
  reg [31:0] _RAND_63;
  reg [31:0] _RAND_64;
  reg [31:0] _RAND_65;
  reg [31:0] _RAND_66;
  reg [31:0] _RAND_67;
  reg [31:0] _RAND_68;
  reg [31:0] _RAND_69;
  reg [31:0] _RAND_70;
  reg [31:0] _RAND_71;
  reg [31:0] _RAND_72;
  reg [31:0] _RAND_73;
  reg [31:0] _RAND_74;
  reg [31:0] _RAND_75;
  reg [31:0] _RAND_76;
  reg [31:0] _RAND_77;
  reg [31:0] _RAND_78;
  reg [31:0] _RAND_79;
  reg [31:0] _RAND_80;
  reg [31:0] _RAND_81;
  reg [31:0] _RAND_82;
  reg [31:0] _RAND_83;
  reg [31:0] _RAND_84;
  reg [31:0] _RAND_85;
  reg [31:0] _RAND_86;
  reg [31:0] _RAND_87;
  reg [31:0] _RAND_88;
  reg [31:0] _RAND_89;
  reg [31:0] _RAND_90;
  reg [31:0] _RAND_91;
  reg [31:0] _RAND_92;
  reg [31:0] _RAND_93;
  reg [31:0] _RAND_94;
  reg [31:0] _RAND_95;
  reg [31:0] _RAND_96;
  reg [31:0] _RAND_97;
  reg [31:0] _RAND_98;
  reg [31:0] _RAND_99;
  reg [31:0] _RAND_100;
  reg [31:0] _RAND_101;
  reg [31:0] _RAND_102;
  reg [31:0] _RAND_103;
  reg [31:0] _RAND_104;
  reg [31:0] _RAND_105;
  reg [31:0] _RAND_106;
  reg [31:0] _RAND_107;
  reg [63:0] _RAND_108;
  reg [63:0] _RAND_109;
  reg [31:0] _RAND_110;
  reg [31:0] _RAND_111;
  reg [31:0] _RAND_112;
  reg [31:0] _RAND_113;
  reg [31:0] _RAND_114;
`endif // RANDOMIZE_REG_INIT
  reg [7:0] sw_length; // @[mult_loom.scala 26:38]
  reg [15:0] activations_0; // @[mult_loom.scala 27:30]
  reg [15:0] activations_1; // @[mult_loom.scala 27:30]
  reg [15:0] activations_2; // @[mult_loom.scala 27:30]
  reg [15:0] activations_3; // @[mult_loom.scala 27:30]
  reg [15:0] activations_4; // @[mult_loom.scala 27:30]
  reg [15:0] activations_5; // @[mult_loom.scala 27:30]
  reg [15:0] activations_6; // @[mult_loom.scala 27:30]
  reg [15:0] activations_7; // @[mult_loom.scala 27:30]
  reg [15:0] activations_8; // @[mult_loom.scala 27:30]
  reg [15:0] activations_9; // @[mult_loom.scala 27:30]
  reg [15:0] activations_10; // @[mult_loom.scala 27:30]
  reg [15:0] activations_11; // @[mult_loom.scala 27:30]
  reg [15:0] activations_12; // @[mult_loom.scala 27:30]
  reg [15:0] activations_13; // @[mult_loom.scala 27:30]
  reg [15:0] activations_14; // @[mult_loom.scala 27:30]
  reg [15:0] activations_15; // @[mult_loom.scala 27:30]
  reg [15:0] weight_0; // @[mult_loom.scala 29:30]
  reg [15:0] weight_1; // @[mult_loom.scala 29:30]
  reg [15:0] weight_2; // @[mult_loom.scala 29:30]
  reg [15:0] weight_3; // @[mult_loom.scala 29:30]
  reg [15:0] weight_4; // @[mult_loom.scala 29:30]
  reg [15:0] weight_5; // @[mult_loom.scala 29:30]
  reg [15:0] weight_6; // @[mult_loom.scala 29:30]
  reg [15:0] weight_7; // @[mult_loom.scala 29:30]
  reg [15:0] weight_8; // @[mult_loom.scala 29:30]
  reg [15:0] weight_9; // @[mult_loom.scala 29:30]
  reg [15:0] weight_10; // @[mult_loom.scala 29:30]
  reg [15:0] weight_11; // @[mult_loom.scala 29:30]
  reg [15:0] weight_12; // @[mult_loom.scala 29:30]
  reg [15:0] weight_13; // @[mult_loom.scala 29:30]
  reg [15:0] weight_14; // @[mult_loom.scala 29:30]
  reg [15:0] weight_15; // @[mult_loom.scala 29:30]
  reg [15:0] init_activations_0; // @[mult_loom.scala 30:30]
  reg [15:0] init_activations_1; // @[mult_loom.scala 30:30]
  reg [15:0] init_activations_2; // @[mult_loom.scala 30:30]
  reg [15:0] init_activations_3; // @[mult_loom.scala 30:30]
  reg [15:0] init_activations_4; // @[mult_loom.scala 30:30]
  reg [15:0] init_activations_5; // @[mult_loom.scala 30:30]
  reg [15:0] init_activations_6; // @[mult_loom.scala 30:30]
  reg [15:0] init_activations_7; // @[mult_loom.scala 30:30]
  reg [15:0] init_activations_8; // @[mult_loom.scala 30:30]
  reg [15:0] init_activations_9; // @[mult_loom.scala 30:30]
  reg [15:0] init_activations_10; // @[mult_loom.scala 30:30]
  reg [15:0] init_activations_11; // @[mult_loom.scala 30:30]
  reg [15:0] init_activations_12; // @[mult_loom.scala 30:30]
  reg [15:0] init_activations_13; // @[mult_loom.scala 30:30]
  reg [15:0] init_activations_14; // @[mult_loom.scala 30:30]
  reg [15:0] init_activations_15; // @[mult_loom.scala 30:30]
  reg  ser_act_0; // @[mult_loom.scala 32:38]
  reg  ser_act_1; // @[mult_loom.scala 32:38]
  reg  ser_act_2; // @[mult_loom.scala 32:38]
  reg  ser_act_3; // @[mult_loom.scala 32:38]
  reg  ser_act_4; // @[mult_loom.scala 32:38]
  reg  ser_act_5; // @[mult_loom.scala 32:38]
  reg  ser_act_6; // @[mult_loom.scala 32:38]
  reg  ser_act_7; // @[mult_loom.scala 32:38]
  reg  ser_act_8; // @[mult_loom.scala 32:38]
  reg  ser_act_9; // @[mult_loom.scala 32:38]
  reg  ser_act_10; // @[mult_loom.scala 32:38]
  reg  ser_act_11; // @[mult_loom.scala 32:38]
  reg  ser_act_12; // @[mult_loom.scala 32:38]
  reg  ser_act_13; // @[mult_loom.scala 32:38]
  reg  ser_act_14; // @[mult_loom.scala 32:38]
  reg  ser_act_15; // @[mult_loom.scala 32:38]
  reg  ser_weight_0; // @[mult_loom.scala 33:38]
  reg  ser_weight_1; // @[mult_loom.scala 33:38]
  reg  ser_weight_2; // @[mult_loom.scala 33:38]
  reg  ser_weight_3; // @[mult_loom.scala 33:38]
  reg  ser_weight_4; // @[mult_loom.scala 33:38]
  reg  ser_weight_5; // @[mult_loom.scala 33:38]
  reg  ser_weight_6; // @[mult_loom.scala 33:38]
  reg  ser_weight_7; // @[mult_loom.scala 33:38]
  reg  ser_weight_8; // @[mult_loom.scala 33:38]
  reg  ser_weight_9; // @[mult_loom.scala 33:38]
  reg  ser_weight_10; // @[mult_loom.scala 33:38]
  reg  ser_weight_11; // @[mult_loom.scala 33:38]
  reg  ser_weight_12; // @[mult_loom.scala 33:38]
  reg  ser_weight_13; // @[mult_loom.scala 33:38]
  reg  ser_weight_14; // @[mult_loom.scala 33:38]
  reg  ser_weight_15; // @[mult_loom.scala 33:38]
  reg  and_result_0; // @[mult_loom.scala 34:38]
  reg  and_result_1; // @[mult_loom.scala 34:38]
  reg  and_result_2; // @[mult_loom.scala 34:38]
  reg  and_result_3; // @[mult_loom.scala 34:38]
  reg  and_result_4; // @[mult_loom.scala 34:38]
  reg  and_result_5; // @[mult_loom.scala 34:38]
  reg  and_result_6; // @[mult_loom.scala 34:38]
  reg  and_result_7; // @[mult_loom.scala 34:38]
  reg  and_result_8; // @[mult_loom.scala 34:38]
  reg  and_result_9; // @[mult_loom.scala 34:38]
  reg  and_result_10; // @[mult_loom.scala 34:38]
  reg  and_result_11; // @[mult_loom.scala 34:38]
  reg  and_result_12; // @[mult_loom.scala 34:38]
  reg  and_result_13; // @[mult_loom.scala 34:38]
  reg  and_result_14; // @[mult_loom.scala 34:38]
  reg  and_result_15; // @[mult_loom.scala 34:38]
  reg [15:0] sum_result; // @[mult_loom.scala 35:38]
  reg [7:0] act_update_counter; // @[mult_loom.scala 37:31]
  reg [7:0] act_update_counter_6; // @[mult_loom.scala 39:33]
  reg [7:0] wgt_update_counter; // @[mult_loom.scala 40:31]
  reg  update_wgt; // @[mult_loom.scala 42:31]
  reg  update_wgt_1; // @[mult_loom.scala 43:31]
  reg  update_wgt_2; // @[mult_loom.scala 44:31]
  reg  update_wgt_3; // @[mult_loom.scala 45:31]
  reg  update_wgt_4; // @[mult_loom.scala 46:31]
  reg  update_wgt_5; // @[mult_loom.scala 47:31]
  reg  update_wgt_6; // @[mult_loom.scala 48:31]
  reg [35:0] accum1_result; // @[mult_loom.scala 52:30]
  reg [35:0] accum2_result; // @[mult_loom.scala 53:30]
  wire [7:0] _T_1 = sw_length - 8'h1; // @[mult_loom.scala 88:48]
  wire  _T_2 = act_update_counter == _T_1; // @[mult_loom.scala 88:33]
  wire [7:0] _act_update_counter_T_1 = act_update_counter + 8'h1; // @[mult_loom.scala 95:58]
  reg [7:0] act_update_counter_6_r; // @[Reg.scala 15:16]
  reg [7:0] act_update_counter_6_r_1; // @[Reg.scala 15:16]
  reg [7:0] act_update_counter_6_r_2; // @[Reg.scala 15:16]
  reg [7:0] act_update_counter_6_r_3; // @[Reg.scala 15:16]
  reg [7:0] act_update_counter_6_r_4; // @[Reg.scala 15:16]
  wire [35:0] _GEN_114 = io_reset ? 36'h0 : accum2_result; // @[mult_loom.scala 66:9 mult_loom.scala 84:31 mult_loom.scala 53:30]
  wire [14:0] weight_update = weight_0[15:1]; // @[mult_loom.scala 122:55]
  wire [14:0] weight_update_1 = weight_1[15:1]; // @[mult_loom.scala 122:55]
  wire [14:0] weight_update_2 = weight_2[15:1]; // @[mult_loom.scala 122:55]
  wire [14:0] weight_update_3 = weight_3[15:1]; // @[mult_loom.scala 122:55]
  wire [14:0] weight_update_4 = weight_4[15:1]; // @[mult_loom.scala 122:55]
  wire [14:0] weight_update_5 = weight_5[15:1]; // @[mult_loom.scala 122:55]
  wire [14:0] weight_update_6 = weight_6[15:1]; // @[mult_loom.scala 122:55]
  wire [14:0] weight_update_7 = weight_7[15:1]; // @[mult_loom.scala 122:55]
  wire [14:0] weight_update_8 = weight_8[15:1]; // @[mult_loom.scala 122:55]
  wire [14:0] weight_update_9 = weight_9[15:1]; // @[mult_loom.scala 122:55]
  wire [14:0] weight_update_10 = weight_10[15:1]; // @[mult_loom.scala 122:55]
  wire [14:0] weight_update_11 = weight_11[15:1]; // @[mult_loom.scala 122:55]
  wire [14:0] weight_update_12 = weight_12[15:1]; // @[mult_loom.scala 122:55]
  wire [14:0] weight_update_13 = weight_13[15:1]; // @[mult_loom.scala 122:55]
  wire [14:0] weight_update_14 = weight_14[15:1]; // @[mult_loom.scala 122:55]
  wire [14:0] weight_update_15 = weight_15[15:1]; // @[mult_loom.scala 122:55]
  wire  act_update_hi = activations_0[0]; // @[mult_loom.scala 133:57]
  wire [14:0] act_update_lo = activations_0[15:1]; // @[mult_loom.scala 133:90]
  wire [15:0] act_update = {act_update_hi,act_update_lo}; // @[Cat.scala 30:58]
  wire  _T_3 = activations_0 != 16'h0; // @[mult_loom.scala 135:42]
  wire  act_update_hi_1 = activations_1[0]; // @[mult_loom.scala 133:57]
  wire [14:0] act_update_lo_1 = activations_1[15:1]; // @[mult_loom.scala 133:90]
  wire [15:0] act_update_1 = {act_update_hi_1,act_update_lo_1}; // @[Cat.scala 30:58]
  wire  _T_4 = activations_1 != 16'h0; // @[mult_loom.scala 135:42]
  wire  act_update_hi_2 = activations_2[0]; // @[mult_loom.scala 133:57]
  wire [14:0] act_update_lo_2 = activations_2[15:1]; // @[mult_loom.scala 133:90]
  wire [15:0] act_update_2 = {act_update_hi_2,act_update_lo_2}; // @[Cat.scala 30:58]
  wire  _T_5 = activations_2 != 16'h0; // @[mult_loom.scala 135:42]
  wire  act_update_hi_3 = activations_3[0]; // @[mult_loom.scala 133:57]
  wire [14:0] act_update_lo_3 = activations_3[15:1]; // @[mult_loom.scala 133:90]
  wire [15:0] act_update_3 = {act_update_hi_3,act_update_lo_3}; // @[Cat.scala 30:58]
  wire  _T_6 = activations_3 != 16'h0; // @[mult_loom.scala 135:42]
  wire  act_update_hi_4 = activations_4[0]; // @[mult_loom.scala 133:57]
  wire [14:0] act_update_lo_4 = activations_4[15:1]; // @[mult_loom.scala 133:90]
  wire [15:0] act_update_4 = {act_update_hi_4,act_update_lo_4}; // @[Cat.scala 30:58]
  wire  _T_7 = activations_4 != 16'h0; // @[mult_loom.scala 135:42]
  wire  act_update_hi_5 = activations_5[0]; // @[mult_loom.scala 133:57]
  wire [14:0] act_update_lo_5 = activations_5[15:1]; // @[mult_loom.scala 133:90]
  wire [15:0] act_update_5 = {act_update_hi_5,act_update_lo_5}; // @[Cat.scala 30:58]
  wire  _T_8 = activations_5 != 16'h0; // @[mult_loom.scala 135:42]
  wire  act_update_hi_6 = activations_6[0]; // @[mult_loom.scala 133:57]
  wire [14:0] act_update_lo_6 = activations_6[15:1]; // @[mult_loom.scala 133:90]
  wire [15:0] act_update_6 = {act_update_hi_6,act_update_lo_6}; // @[Cat.scala 30:58]
  wire  _T_9 = activations_6 != 16'h0; // @[mult_loom.scala 135:42]
  wire  act_update_hi_7 = activations_7[0]; // @[mult_loom.scala 133:57]
  wire [14:0] act_update_lo_7 = activations_7[15:1]; // @[mult_loom.scala 133:90]
  wire [15:0] act_update_7 = {act_update_hi_7,act_update_lo_7}; // @[Cat.scala 30:58]
  wire  _T_10 = activations_7 != 16'h0; // @[mult_loom.scala 135:42]
  wire  act_update_hi_8 = activations_8[0]; // @[mult_loom.scala 133:57]
  wire [14:0] act_update_lo_8 = activations_8[15:1]; // @[mult_loom.scala 133:90]
  wire [15:0] act_update_8 = {act_update_hi_8,act_update_lo_8}; // @[Cat.scala 30:58]
  wire  _T_11 = activations_8 != 16'h0; // @[mult_loom.scala 135:42]
  wire  act_update_hi_9 = activations_9[0]; // @[mult_loom.scala 133:57]
  wire [14:0] act_update_lo_9 = activations_9[15:1]; // @[mult_loom.scala 133:90]
  wire [15:0] act_update_9 = {act_update_hi_9,act_update_lo_9}; // @[Cat.scala 30:58]
  wire  _T_12 = activations_9 != 16'h0; // @[mult_loom.scala 135:42]
  wire  act_update_hi_10 = activations_10[0]; // @[mult_loom.scala 133:57]
  wire [14:0] act_update_lo_10 = activations_10[15:1]; // @[mult_loom.scala 133:90]
  wire [15:0] act_update_10 = {act_update_hi_10,act_update_lo_10}; // @[Cat.scala 30:58]
  wire  _T_13 = activations_10 != 16'h0; // @[mult_loom.scala 135:42]
  wire  act_update_hi_11 = activations_11[0]; // @[mult_loom.scala 133:57]
  wire [14:0] act_update_lo_11 = activations_11[15:1]; // @[mult_loom.scala 133:90]
  wire [15:0] act_update_11 = {act_update_hi_11,act_update_lo_11}; // @[Cat.scala 30:58]
  wire  _T_14 = activations_11 != 16'h0; // @[mult_loom.scala 135:42]
  wire  act_update_hi_12 = activations_12[0]; // @[mult_loom.scala 133:57]
  wire [14:0] act_update_lo_12 = activations_12[15:1]; // @[mult_loom.scala 133:90]
  wire [15:0] act_update_12 = {act_update_hi_12,act_update_lo_12}; // @[Cat.scala 30:58]
  wire  _T_15 = activations_12 != 16'h0; // @[mult_loom.scala 135:42]
  wire  act_update_hi_13 = activations_13[0]; // @[mult_loom.scala 133:57]
  wire [14:0] act_update_lo_13 = activations_13[15:1]; // @[mult_loom.scala 133:90]
  wire [15:0] act_update_13 = {act_update_hi_13,act_update_lo_13}; // @[Cat.scala 30:58]
  wire  _T_16 = activations_13 != 16'h0; // @[mult_loom.scala 135:42]
  wire  act_update_hi_14 = activations_14[0]; // @[mult_loom.scala 133:57]
  wire [14:0] act_update_lo_14 = activations_14[15:1]; // @[mult_loom.scala 133:90]
  wire [15:0] act_update_14 = {act_update_hi_14,act_update_lo_14}; // @[Cat.scala 30:58]
  wire  _T_17 = activations_14 != 16'h0; // @[mult_loom.scala 135:42]
  wire  act_update_hi_15 = activations_15[0]; // @[mult_loom.scala 133:57]
  wire [14:0] act_update_lo_15 = activations_15[15:1]; // @[mult_loom.scala 133:90]
  wire [15:0] act_update_15 = {act_update_hi_15,act_update_lo_15}; // @[Cat.scala 30:58]
  wire  _T_18 = activations_15 != 16'h0; // @[mult_loom.scala 135:42]
  wire [1:0] _sum_result_T = and_result_0 + and_result_1; // @[mult_loom.scala 157:47]
  wire [1:0] _GEN_183 = {{1'd0}, and_result_2}; // @[mult_loom.scala 157:47]
  wire [2:0] _sum_result_T_1 = _sum_result_T + _GEN_183; // @[mult_loom.scala 157:47]
  wire [2:0] _GEN_184 = {{2'd0}, and_result_3}; // @[mult_loom.scala 157:47]
  wire [3:0] _sum_result_T_2 = _sum_result_T_1 + _GEN_184; // @[mult_loom.scala 157:47]
  wire [3:0] _GEN_185 = {{3'd0}, and_result_4}; // @[mult_loom.scala 157:47]
  wire [4:0] _sum_result_T_3 = _sum_result_T_2 + _GEN_185; // @[mult_loom.scala 157:47]
  wire [4:0] _GEN_186 = {{4'd0}, and_result_5}; // @[mult_loom.scala 157:47]
  wire [5:0] _sum_result_T_4 = _sum_result_T_3 + _GEN_186; // @[mult_loom.scala 157:47]
  wire [5:0] _GEN_187 = {{5'd0}, and_result_6}; // @[mult_loom.scala 157:47]
  wire [6:0] _sum_result_T_5 = _sum_result_T_4 + _GEN_187; // @[mult_loom.scala 157:47]
  wire [6:0] _GEN_188 = {{6'd0}, and_result_7}; // @[mult_loom.scala 157:47]
  wire [7:0] _sum_result_T_6 = _sum_result_T_5 + _GEN_188; // @[mult_loom.scala 157:47]
  wire [7:0] _GEN_189 = {{7'd0}, and_result_8}; // @[mult_loom.scala 157:47]
  wire [8:0] _sum_result_T_7 = _sum_result_T_6 + _GEN_189; // @[mult_loom.scala 157:47]
  wire [8:0] _GEN_190 = {{8'd0}, and_result_9}; // @[mult_loom.scala 157:47]
  wire [9:0] _sum_result_T_8 = _sum_result_T_7 + _GEN_190; // @[mult_loom.scala 157:47]
  wire [9:0] _GEN_191 = {{9'd0}, and_result_10}; // @[mult_loom.scala 157:47]
  wire [10:0] _sum_result_T_9 = _sum_result_T_8 + _GEN_191; // @[mult_loom.scala 157:47]
  wire [10:0] _GEN_192 = {{10'd0}, and_result_11}; // @[mult_loom.scala 157:47]
  wire [11:0] _sum_result_T_10 = _sum_result_T_9 + _GEN_192; // @[mult_loom.scala 157:47]
  wire [11:0] _GEN_193 = {{11'd0}, and_result_12}; // @[mult_loom.scala 157:47]
  wire [12:0] _sum_result_T_11 = _sum_result_T_10 + _GEN_193; // @[mult_loom.scala 157:47]
  wire [12:0] _GEN_194 = {{12'd0}, and_result_13}; // @[mult_loom.scala 157:47]
  wire [13:0] _sum_result_T_12 = _sum_result_T_11 + _GEN_194; // @[mult_loom.scala 157:47]
  wire [13:0] _GEN_195 = {{13'd0}, and_result_14}; // @[mult_loom.scala 157:47]
  wire [14:0] _sum_result_T_13 = _sum_result_T_12 + _GEN_195; // @[mult_loom.scala 157:47]
  wire [14:0] _GEN_196 = {{14'd0}, and_result_15}; // @[mult_loom.scala 157:47]
  wire  _T_21 = wgt_update_counter == _T_1; // @[mult_loom.scala 164:33]
  wire [7:0] _wgt_update_counter_T_1 = wgt_update_counter + 8'h1; // @[mult_loom.scala 171:50]
  wire [290:0] _GEN_197 = {{255'd0}, accum1_result}; // @[mult_loom.scala 174:41]
  wire [290:0] _accum2_result_T = _GEN_197 << wgt_update_counter; // @[mult_loom.scala 174:41]
  wire [290:0] _GEN_198 = {{255'd0}, accum2_result}; // @[mult_loom.scala 174:64]
  wire [290:0] _accum2_result_T_2 = _accum2_result_T + _GEN_198; // @[mult_loom.scala 174:64]
  wire [270:0] _GEN_199 = {{255'd0}, sum_result}; // @[mult_loom.scala 179:47]
  wire [270:0] _accum1_result_T = _GEN_199 << act_update_counter_6; // @[mult_loom.scala 179:47]
  wire [270:0] _GEN_200 = {{235'd0}, accum1_result}; // @[mult_loom.scala 179:72]
  wire [270:0] _accum1_result_T_2 = _accum1_result_T + _GEN_200; // @[mult_loom.scala 179:72]
  wire [270:0] _GEN_180 = update_wgt_6 ? {{255'd0}, sum_result} : _accum1_result_T_2; // @[mult_loom.scala 161:9 mult_loom.scala 162:23 mult_loom.scala 179:31]
  wire [290:0] _GEN_182 = update_wgt_6 ? _accum2_result_T_2 : {{255'd0}, _GEN_114}; // @[mult_loom.scala 161:9 mult_loom.scala 174:23]
  assign io_outputProduct = accum2_result[31:0]; // @[mult_loom.scala 182:26]
  always @(posedge clock) begin
    if (io_loadingValues) begin // @[mult_loom.scala 57:28]
      sw_length <= io_sw_length; // @[mult_loom.scala 61:19]
    end
    if (_T_3) begin // @[mult_loom.scala 136:9]
      if (io_reset) begin // @[mult_loom.scala 138:9]
        activations_0 <= 16'h0; // @[mult_loom.scala 138:38]
      end else if (update_wgt_2) begin // @[mult_loom.scala 142:9]
        activations_0 <= init_activations_0; // @[mult_loom.scala 142:38]
      end else begin
        activations_0 <= act_update; // @[mult_loom.scala 144:38]
      end
    end else if (io_reset) begin // @[mult_loom.scala 66:9]
      activations_0 <= 16'h0; // @[mult_loom.scala 78:49]
    end else if (io_loadingValues) begin // @[mult_loom.scala 57:28]
      activations_0 <= io_activations_0; // @[mult_loom.scala 58:17]
    end
    if (_T_4) begin // @[mult_loom.scala 136:9]
      if (io_reset) begin // @[mult_loom.scala 138:9]
        activations_1 <= 16'h0; // @[mult_loom.scala 138:38]
      end else if (update_wgt_2) begin // @[mult_loom.scala 142:9]
        activations_1 <= init_activations_1; // @[mult_loom.scala 142:38]
      end else begin
        activations_1 <= act_update_1; // @[mult_loom.scala 144:38]
      end
    end else if (io_reset) begin // @[mult_loom.scala 66:9]
      activations_1 <= 16'h0; // @[mult_loom.scala 78:49]
    end else if (io_loadingValues) begin // @[mult_loom.scala 57:28]
      activations_1 <= io_activations_1; // @[mult_loom.scala 58:17]
    end
    if (_T_5) begin // @[mult_loom.scala 136:9]
      if (io_reset) begin // @[mult_loom.scala 138:9]
        activations_2 <= 16'h0; // @[mult_loom.scala 138:38]
      end else if (update_wgt_2) begin // @[mult_loom.scala 142:9]
        activations_2 <= init_activations_2; // @[mult_loom.scala 142:38]
      end else begin
        activations_2 <= act_update_2; // @[mult_loom.scala 144:38]
      end
    end else if (io_reset) begin // @[mult_loom.scala 66:9]
      activations_2 <= 16'h0; // @[mult_loom.scala 78:49]
    end else if (io_loadingValues) begin // @[mult_loom.scala 57:28]
      activations_2 <= io_activations_2; // @[mult_loom.scala 58:17]
    end
    if (_T_6) begin // @[mult_loom.scala 136:9]
      if (io_reset) begin // @[mult_loom.scala 138:9]
        activations_3 <= 16'h0; // @[mult_loom.scala 138:38]
      end else if (update_wgt_2) begin // @[mult_loom.scala 142:9]
        activations_3 <= init_activations_3; // @[mult_loom.scala 142:38]
      end else begin
        activations_3 <= act_update_3; // @[mult_loom.scala 144:38]
      end
    end else if (io_reset) begin // @[mult_loom.scala 66:9]
      activations_3 <= 16'h0; // @[mult_loom.scala 78:49]
    end else if (io_loadingValues) begin // @[mult_loom.scala 57:28]
      activations_3 <= io_activations_3; // @[mult_loom.scala 58:17]
    end
    if (_T_7) begin // @[mult_loom.scala 136:9]
      if (io_reset) begin // @[mult_loom.scala 138:9]
        activations_4 <= 16'h0; // @[mult_loom.scala 138:38]
      end else if (update_wgt_2) begin // @[mult_loom.scala 142:9]
        activations_4 <= init_activations_4; // @[mult_loom.scala 142:38]
      end else begin
        activations_4 <= act_update_4; // @[mult_loom.scala 144:38]
      end
    end else if (io_reset) begin // @[mult_loom.scala 66:9]
      activations_4 <= 16'h0; // @[mult_loom.scala 78:49]
    end else if (io_loadingValues) begin // @[mult_loom.scala 57:28]
      activations_4 <= io_activations_4; // @[mult_loom.scala 58:17]
    end
    if (_T_8) begin // @[mult_loom.scala 136:9]
      if (io_reset) begin // @[mult_loom.scala 138:9]
        activations_5 <= 16'h0; // @[mult_loom.scala 138:38]
      end else if (update_wgt_2) begin // @[mult_loom.scala 142:9]
        activations_5 <= init_activations_5; // @[mult_loom.scala 142:38]
      end else begin
        activations_5 <= act_update_5; // @[mult_loom.scala 144:38]
      end
    end else if (io_reset) begin // @[mult_loom.scala 66:9]
      activations_5 <= 16'h0; // @[mult_loom.scala 78:49]
    end else if (io_loadingValues) begin // @[mult_loom.scala 57:28]
      activations_5 <= io_activations_5; // @[mult_loom.scala 58:17]
    end
    if (_T_9) begin // @[mult_loom.scala 136:9]
      if (io_reset) begin // @[mult_loom.scala 138:9]
        activations_6 <= 16'h0; // @[mult_loom.scala 138:38]
      end else if (update_wgt_2) begin // @[mult_loom.scala 142:9]
        activations_6 <= init_activations_6; // @[mult_loom.scala 142:38]
      end else begin
        activations_6 <= act_update_6; // @[mult_loom.scala 144:38]
      end
    end else if (io_reset) begin // @[mult_loom.scala 66:9]
      activations_6 <= 16'h0; // @[mult_loom.scala 78:49]
    end else if (io_loadingValues) begin // @[mult_loom.scala 57:28]
      activations_6 <= io_activations_6; // @[mult_loom.scala 58:17]
    end
    if (_T_10) begin // @[mult_loom.scala 136:9]
      if (io_reset) begin // @[mult_loom.scala 138:9]
        activations_7 <= 16'h0; // @[mult_loom.scala 138:38]
      end else if (update_wgt_2) begin // @[mult_loom.scala 142:9]
        activations_7 <= init_activations_7; // @[mult_loom.scala 142:38]
      end else begin
        activations_7 <= act_update_7; // @[mult_loom.scala 144:38]
      end
    end else if (io_reset) begin // @[mult_loom.scala 66:9]
      activations_7 <= 16'h0; // @[mult_loom.scala 78:49]
    end else if (io_loadingValues) begin // @[mult_loom.scala 57:28]
      activations_7 <= io_activations_7; // @[mult_loom.scala 58:17]
    end
    if (_T_11) begin // @[mult_loom.scala 136:9]
      if (io_reset) begin // @[mult_loom.scala 138:9]
        activations_8 <= 16'h0; // @[mult_loom.scala 138:38]
      end else if (update_wgt_2) begin // @[mult_loom.scala 142:9]
        activations_8 <= init_activations_8; // @[mult_loom.scala 142:38]
      end else begin
        activations_8 <= act_update_8; // @[mult_loom.scala 144:38]
      end
    end else if (io_reset) begin // @[mult_loom.scala 66:9]
      activations_8 <= 16'h0; // @[mult_loom.scala 78:49]
    end else if (io_loadingValues) begin // @[mult_loom.scala 57:28]
      activations_8 <= io_activations_8; // @[mult_loom.scala 58:17]
    end
    if (_T_12) begin // @[mult_loom.scala 136:9]
      if (io_reset) begin // @[mult_loom.scala 138:9]
        activations_9 <= 16'h0; // @[mult_loom.scala 138:38]
      end else if (update_wgt_2) begin // @[mult_loom.scala 142:9]
        activations_9 <= init_activations_9; // @[mult_loom.scala 142:38]
      end else begin
        activations_9 <= act_update_9; // @[mult_loom.scala 144:38]
      end
    end else if (io_reset) begin // @[mult_loom.scala 66:9]
      activations_9 <= 16'h0; // @[mult_loom.scala 78:49]
    end else if (io_loadingValues) begin // @[mult_loom.scala 57:28]
      activations_9 <= io_activations_9; // @[mult_loom.scala 58:17]
    end
    if (_T_13) begin // @[mult_loom.scala 136:9]
      if (io_reset) begin // @[mult_loom.scala 138:9]
        activations_10 <= 16'h0; // @[mult_loom.scala 138:38]
      end else if (update_wgt_2) begin // @[mult_loom.scala 142:9]
        activations_10 <= init_activations_10; // @[mult_loom.scala 142:38]
      end else begin
        activations_10 <= act_update_10; // @[mult_loom.scala 144:38]
      end
    end else if (io_reset) begin // @[mult_loom.scala 66:9]
      activations_10 <= 16'h0; // @[mult_loom.scala 78:49]
    end else if (io_loadingValues) begin // @[mult_loom.scala 57:28]
      activations_10 <= io_activations_10; // @[mult_loom.scala 58:17]
    end
    if (_T_14) begin // @[mult_loom.scala 136:9]
      if (io_reset) begin // @[mult_loom.scala 138:9]
        activations_11 <= 16'h0; // @[mult_loom.scala 138:38]
      end else if (update_wgt_2) begin // @[mult_loom.scala 142:9]
        activations_11 <= init_activations_11; // @[mult_loom.scala 142:38]
      end else begin
        activations_11 <= act_update_11; // @[mult_loom.scala 144:38]
      end
    end else if (io_reset) begin // @[mult_loom.scala 66:9]
      activations_11 <= 16'h0; // @[mult_loom.scala 78:49]
    end else if (io_loadingValues) begin // @[mult_loom.scala 57:28]
      activations_11 <= io_activations_11; // @[mult_loom.scala 58:17]
    end
    if (_T_15) begin // @[mult_loom.scala 136:9]
      if (io_reset) begin // @[mult_loom.scala 138:9]
        activations_12 <= 16'h0; // @[mult_loom.scala 138:38]
      end else if (update_wgt_2) begin // @[mult_loom.scala 142:9]
        activations_12 <= init_activations_12; // @[mult_loom.scala 142:38]
      end else begin
        activations_12 <= act_update_12; // @[mult_loom.scala 144:38]
      end
    end else if (io_reset) begin // @[mult_loom.scala 66:9]
      activations_12 <= 16'h0; // @[mult_loom.scala 78:49]
    end else if (io_loadingValues) begin // @[mult_loom.scala 57:28]
      activations_12 <= io_activations_12; // @[mult_loom.scala 58:17]
    end
    if (_T_16) begin // @[mult_loom.scala 136:9]
      if (io_reset) begin // @[mult_loom.scala 138:9]
        activations_13 <= 16'h0; // @[mult_loom.scala 138:38]
      end else if (update_wgt_2) begin // @[mult_loom.scala 142:9]
        activations_13 <= init_activations_13; // @[mult_loom.scala 142:38]
      end else begin
        activations_13 <= act_update_13; // @[mult_loom.scala 144:38]
      end
    end else if (io_reset) begin // @[mult_loom.scala 66:9]
      activations_13 <= 16'h0; // @[mult_loom.scala 78:49]
    end else if (io_loadingValues) begin // @[mult_loom.scala 57:28]
      activations_13 <= io_activations_13; // @[mult_loom.scala 58:17]
    end
    if (_T_17) begin // @[mult_loom.scala 136:9]
      if (io_reset) begin // @[mult_loom.scala 138:9]
        activations_14 <= 16'h0; // @[mult_loom.scala 138:38]
      end else if (update_wgt_2) begin // @[mult_loom.scala 142:9]
        activations_14 <= init_activations_14; // @[mult_loom.scala 142:38]
      end else begin
        activations_14 <= act_update_14; // @[mult_loom.scala 144:38]
      end
    end else if (io_reset) begin // @[mult_loom.scala 66:9]
      activations_14 <= 16'h0; // @[mult_loom.scala 78:49]
    end else if (io_loadingValues) begin // @[mult_loom.scala 57:28]
      activations_14 <= io_activations_14; // @[mult_loom.scala 58:17]
    end
    if (_T_18) begin // @[mult_loom.scala 136:9]
      if (io_reset) begin // @[mult_loom.scala 138:9]
        activations_15 <= 16'h0; // @[mult_loom.scala 138:38]
      end else if (update_wgt_2) begin // @[mult_loom.scala 142:9]
        activations_15 <= init_activations_15; // @[mult_loom.scala 142:38]
      end else begin
        activations_15 <= act_update_15; // @[mult_loom.scala 144:38]
      end
    end else if (io_reset) begin // @[mult_loom.scala 66:9]
      activations_15 <= 16'h0; // @[mult_loom.scala 78:49]
    end else if (io_loadingValues) begin // @[mult_loom.scala 57:28]
      activations_15 <= io_activations_15; // @[mult_loom.scala 58:17]
    end
    if (update_wgt_2) begin // @[mult_loom.scala 126:9]
      weight_0 <= {{1'd0}, weight_update}; // @[mult_loom.scala 127:35]
    end else if (io_reset) begin // @[mult_loom.scala 66:9]
      weight_0 <= 16'h0; // @[mult_loom.scala 79:44]
    end else if (io_loadingValues) begin // @[mult_loom.scala 57:28]
      weight_0 <= io_weight_0; // @[mult_loom.scala 62:12]
    end
    if (update_wgt_2) begin // @[mult_loom.scala 126:9]
      weight_1 <= {{1'd0}, weight_update_1}; // @[mult_loom.scala 127:35]
    end else if (io_reset) begin // @[mult_loom.scala 66:9]
      weight_1 <= 16'h0; // @[mult_loom.scala 79:44]
    end else if (io_loadingValues) begin // @[mult_loom.scala 57:28]
      weight_1 <= io_weight_1; // @[mult_loom.scala 62:12]
    end
    if (update_wgt_2) begin // @[mult_loom.scala 126:9]
      weight_2 <= {{1'd0}, weight_update_2}; // @[mult_loom.scala 127:35]
    end else if (io_reset) begin // @[mult_loom.scala 66:9]
      weight_2 <= 16'h0; // @[mult_loom.scala 79:44]
    end else if (io_loadingValues) begin // @[mult_loom.scala 57:28]
      weight_2 <= io_weight_2; // @[mult_loom.scala 62:12]
    end
    if (update_wgt_2) begin // @[mult_loom.scala 126:9]
      weight_3 <= {{1'd0}, weight_update_3}; // @[mult_loom.scala 127:35]
    end else if (io_reset) begin // @[mult_loom.scala 66:9]
      weight_3 <= 16'h0; // @[mult_loom.scala 79:44]
    end else if (io_loadingValues) begin // @[mult_loom.scala 57:28]
      weight_3 <= io_weight_3; // @[mult_loom.scala 62:12]
    end
    if (update_wgt_2) begin // @[mult_loom.scala 126:9]
      weight_4 <= {{1'd0}, weight_update_4}; // @[mult_loom.scala 127:35]
    end else if (io_reset) begin // @[mult_loom.scala 66:9]
      weight_4 <= 16'h0; // @[mult_loom.scala 79:44]
    end else if (io_loadingValues) begin // @[mult_loom.scala 57:28]
      weight_4 <= io_weight_4; // @[mult_loom.scala 62:12]
    end
    if (update_wgt_2) begin // @[mult_loom.scala 126:9]
      weight_5 <= {{1'd0}, weight_update_5}; // @[mult_loom.scala 127:35]
    end else if (io_reset) begin // @[mult_loom.scala 66:9]
      weight_5 <= 16'h0; // @[mult_loom.scala 79:44]
    end else if (io_loadingValues) begin // @[mult_loom.scala 57:28]
      weight_5 <= io_weight_5; // @[mult_loom.scala 62:12]
    end
    if (update_wgt_2) begin // @[mult_loom.scala 126:9]
      weight_6 <= {{1'd0}, weight_update_6}; // @[mult_loom.scala 127:35]
    end else if (io_reset) begin // @[mult_loom.scala 66:9]
      weight_6 <= 16'h0; // @[mult_loom.scala 79:44]
    end else if (io_loadingValues) begin // @[mult_loom.scala 57:28]
      weight_6 <= io_weight_6; // @[mult_loom.scala 62:12]
    end
    if (update_wgt_2) begin // @[mult_loom.scala 126:9]
      weight_7 <= {{1'd0}, weight_update_7}; // @[mult_loom.scala 127:35]
    end else if (io_reset) begin // @[mult_loom.scala 66:9]
      weight_7 <= 16'h0; // @[mult_loom.scala 79:44]
    end else if (io_loadingValues) begin // @[mult_loom.scala 57:28]
      weight_7 <= io_weight_7; // @[mult_loom.scala 62:12]
    end
    if (update_wgt_2) begin // @[mult_loom.scala 126:9]
      weight_8 <= {{1'd0}, weight_update_8}; // @[mult_loom.scala 127:35]
    end else if (io_reset) begin // @[mult_loom.scala 66:9]
      weight_8 <= 16'h0; // @[mult_loom.scala 79:44]
    end else if (io_loadingValues) begin // @[mult_loom.scala 57:28]
      weight_8 <= io_weight_8; // @[mult_loom.scala 62:12]
    end
    if (update_wgt_2) begin // @[mult_loom.scala 126:9]
      weight_9 <= {{1'd0}, weight_update_9}; // @[mult_loom.scala 127:35]
    end else if (io_reset) begin // @[mult_loom.scala 66:9]
      weight_9 <= 16'h0; // @[mult_loom.scala 79:44]
    end else if (io_loadingValues) begin // @[mult_loom.scala 57:28]
      weight_9 <= io_weight_9; // @[mult_loom.scala 62:12]
    end
    if (update_wgt_2) begin // @[mult_loom.scala 126:9]
      weight_10 <= {{1'd0}, weight_update_10}; // @[mult_loom.scala 127:35]
    end else if (io_reset) begin // @[mult_loom.scala 66:9]
      weight_10 <= 16'h0; // @[mult_loom.scala 79:44]
    end else if (io_loadingValues) begin // @[mult_loom.scala 57:28]
      weight_10 <= io_weight_10; // @[mult_loom.scala 62:12]
    end
    if (update_wgt_2) begin // @[mult_loom.scala 126:9]
      weight_11 <= {{1'd0}, weight_update_11}; // @[mult_loom.scala 127:35]
    end else if (io_reset) begin // @[mult_loom.scala 66:9]
      weight_11 <= 16'h0; // @[mult_loom.scala 79:44]
    end else if (io_loadingValues) begin // @[mult_loom.scala 57:28]
      weight_11 <= io_weight_11; // @[mult_loom.scala 62:12]
    end
    if (update_wgt_2) begin // @[mult_loom.scala 126:9]
      weight_12 <= {{1'd0}, weight_update_12}; // @[mult_loom.scala 127:35]
    end else if (io_reset) begin // @[mult_loom.scala 66:9]
      weight_12 <= 16'h0; // @[mult_loom.scala 79:44]
    end else if (io_loadingValues) begin // @[mult_loom.scala 57:28]
      weight_12 <= io_weight_12; // @[mult_loom.scala 62:12]
    end
    if (update_wgt_2) begin // @[mult_loom.scala 126:9]
      weight_13 <= {{1'd0}, weight_update_13}; // @[mult_loom.scala 127:35]
    end else if (io_reset) begin // @[mult_loom.scala 66:9]
      weight_13 <= 16'h0; // @[mult_loom.scala 79:44]
    end else if (io_loadingValues) begin // @[mult_loom.scala 57:28]
      weight_13 <= io_weight_13; // @[mult_loom.scala 62:12]
    end
    if (update_wgt_2) begin // @[mult_loom.scala 126:9]
      weight_14 <= {{1'd0}, weight_update_14}; // @[mult_loom.scala 127:35]
    end else if (io_reset) begin // @[mult_loom.scala 66:9]
      weight_14 <= 16'h0; // @[mult_loom.scala 79:44]
    end else if (io_loadingValues) begin // @[mult_loom.scala 57:28]
      weight_14 <= io_weight_14; // @[mult_loom.scala 62:12]
    end
    if (update_wgt_2) begin // @[mult_loom.scala 126:9]
      weight_15 <= {{1'd0}, weight_update_15}; // @[mult_loom.scala 127:35]
    end else if (io_reset) begin // @[mult_loom.scala 66:9]
      weight_15 <= 16'h0; // @[mult_loom.scala 79:44]
    end else if (io_loadingValues) begin // @[mult_loom.scala 57:28]
      weight_15 <= io_weight_15; // @[mult_loom.scala 62:12]
    end
    if (io_loadingValues) begin // @[mult_loom.scala 57:28]
      init_activations_0 <= io_activations_0; // @[mult_loom.scala 59:26]
    end
    if (io_loadingValues) begin // @[mult_loom.scala 57:28]
      init_activations_1 <= io_activations_1; // @[mult_loom.scala 59:26]
    end
    if (io_loadingValues) begin // @[mult_loom.scala 57:28]
      init_activations_2 <= io_activations_2; // @[mult_loom.scala 59:26]
    end
    if (io_loadingValues) begin // @[mult_loom.scala 57:28]
      init_activations_3 <= io_activations_3; // @[mult_loom.scala 59:26]
    end
    if (io_loadingValues) begin // @[mult_loom.scala 57:28]
      init_activations_4 <= io_activations_4; // @[mult_loom.scala 59:26]
    end
    if (io_loadingValues) begin // @[mult_loom.scala 57:28]
      init_activations_5 <= io_activations_5; // @[mult_loom.scala 59:26]
    end
    if (io_loadingValues) begin // @[mult_loom.scala 57:28]
      init_activations_6 <= io_activations_6; // @[mult_loom.scala 59:26]
    end
    if (io_loadingValues) begin // @[mult_loom.scala 57:28]
      init_activations_7 <= io_activations_7; // @[mult_loom.scala 59:26]
    end
    if (io_loadingValues) begin // @[mult_loom.scala 57:28]
      init_activations_8 <= io_activations_8; // @[mult_loom.scala 59:26]
    end
    if (io_loadingValues) begin // @[mult_loom.scala 57:28]
      init_activations_9 <= io_activations_9; // @[mult_loom.scala 59:26]
    end
    if (io_loadingValues) begin // @[mult_loom.scala 57:28]
      init_activations_10 <= io_activations_10; // @[mult_loom.scala 59:26]
    end
    if (io_loadingValues) begin // @[mult_loom.scala 57:28]
      init_activations_11 <= io_activations_11; // @[mult_loom.scala 59:26]
    end
    if (io_loadingValues) begin // @[mult_loom.scala 57:28]
      init_activations_12 <= io_activations_12; // @[mult_loom.scala 59:26]
    end
    if (io_loadingValues) begin // @[mult_loom.scala 57:28]
      init_activations_13 <= io_activations_13; // @[mult_loom.scala 59:26]
    end
    if (io_loadingValues) begin // @[mult_loom.scala 57:28]
      init_activations_14 <= io_activations_14; // @[mult_loom.scala 59:26]
    end
    if (io_loadingValues) begin // @[mult_loom.scala 57:28]
      init_activations_15 <= io_activations_15; // @[mult_loom.scala 59:26]
    end
    ser_act_0 <= activations_0[0]; // @[mult_loom.scala 134:63]
    ser_act_1 <= activations_1[0]; // @[mult_loom.scala 134:63]
    ser_act_2 <= activations_2[0]; // @[mult_loom.scala 134:63]
    ser_act_3 <= activations_3[0]; // @[mult_loom.scala 134:63]
    ser_act_4 <= activations_4[0]; // @[mult_loom.scala 134:63]
    ser_act_5 <= activations_5[0]; // @[mult_loom.scala 134:63]
    ser_act_6 <= activations_6[0]; // @[mult_loom.scala 134:63]
    ser_act_7 <= activations_7[0]; // @[mult_loom.scala 134:63]
    ser_act_8 <= activations_8[0]; // @[mult_loom.scala 134:63]
    ser_act_9 <= activations_9[0]; // @[mult_loom.scala 134:63]
    ser_act_10 <= activations_10[0]; // @[mult_loom.scala 134:63]
    ser_act_11 <= activations_11[0]; // @[mult_loom.scala 134:63]
    ser_act_12 <= activations_12[0]; // @[mult_loom.scala 134:63]
    ser_act_13 <= activations_13[0]; // @[mult_loom.scala 134:63]
    ser_act_14 <= activations_14[0]; // @[mult_loom.scala 134:63]
    ser_act_15 <= activations_15[0]; // @[mult_loom.scala 134:63]
    ser_weight_0 <= weight_0[0]; // @[mult_loom.scala 124:67]
    ser_weight_1 <= weight_1[0]; // @[mult_loom.scala 124:67]
    ser_weight_2 <= weight_2[0]; // @[mult_loom.scala 124:67]
    ser_weight_3 <= weight_3[0]; // @[mult_loom.scala 124:67]
    ser_weight_4 <= weight_4[0]; // @[mult_loom.scala 124:67]
    ser_weight_5 <= weight_5[0]; // @[mult_loom.scala 124:67]
    ser_weight_6 <= weight_6[0]; // @[mult_loom.scala 124:67]
    ser_weight_7 <= weight_7[0]; // @[mult_loom.scala 124:67]
    ser_weight_8 <= weight_8[0]; // @[mult_loom.scala 124:67]
    ser_weight_9 <= weight_9[0]; // @[mult_loom.scala 124:67]
    ser_weight_10 <= weight_10[0]; // @[mult_loom.scala 124:67]
    ser_weight_11 <= weight_11[0]; // @[mult_loom.scala 124:67]
    ser_weight_12 <= weight_12[0]; // @[mult_loom.scala 124:67]
    ser_weight_13 <= weight_13[0]; // @[mult_loom.scala 124:67]
    ser_weight_14 <= weight_14[0]; // @[mult_loom.scala 124:67]
    ser_weight_15 <= weight_15[0]; // @[mult_loom.scala 124:67]
    and_result_0 <= ser_act_0 & ser_weight_0; // @[mult_loom.scala 154:55]
    and_result_1 <= ser_act_1 & ser_weight_1; // @[mult_loom.scala 154:55]
    and_result_2 <= ser_act_2 & ser_weight_2; // @[mult_loom.scala 154:55]
    and_result_3 <= ser_act_3 & ser_weight_3; // @[mult_loom.scala 154:55]
    and_result_4 <= ser_act_4 & ser_weight_4; // @[mult_loom.scala 154:55]
    and_result_5 <= ser_act_5 & ser_weight_5; // @[mult_loom.scala 154:55]
    and_result_6 <= ser_act_6 & ser_weight_6; // @[mult_loom.scala 154:55]
    and_result_7 <= ser_act_7 & ser_weight_7; // @[mult_loom.scala 154:55]
    and_result_8 <= ser_act_8 & ser_weight_8; // @[mult_loom.scala 154:55]
    and_result_9 <= ser_act_9 & ser_weight_9; // @[mult_loom.scala 154:55]
    and_result_10 <= ser_act_10 & ser_weight_10; // @[mult_loom.scala 154:55]
    and_result_11 <= ser_act_11 & ser_weight_11; // @[mult_loom.scala 154:55]
    and_result_12 <= ser_act_12 & ser_weight_12; // @[mult_loom.scala 154:55]
    and_result_13 <= ser_act_13 & ser_weight_13; // @[mult_loom.scala 154:55]
    and_result_14 <= ser_act_14 & ser_weight_14; // @[mult_loom.scala 154:55]
    and_result_15 <= ser_act_15 & ser_weight_15; // @[mult_loom.scala 154:55]
    sum_result <= _sum_result_T_13 + _GEN_196; // @[mult_loom.scala 157:47]
    if (io_reset) begin // @[mult_loom.scala 66:9]
      act_update_counter <= 8'h0; // @[mult_loom.scala 68:36]
    end else if (_T_2) begin // @[mult_loom.scala 89:9]
      act_update_counter <= 8'h0; // @[mult_loom.scala 90:36]
    end else begin
      act_update_counter <= _act_update_counter_T_1; // @[mult_loom.scala 95:36]
    end
    if (io_reset) begin // @[mult_loom.scala 66:9]
      act_update_counter_6 <= 8'h0; // @[mult_loom.scala 82:38]
    end else begin
      act_update_counter_6 <= act_update_counter_6_r_4; // @[mult_loom.scala 104:30]
    end
    if (update_wgt_6) begin // @[mult_loom.scala 161:9]
      if (_T_21) begin // @[mult_loom.scala 165:9]
        wgt_update_counter <= 8'h0; // @[mult_loom.scala 166:28]
      end else begin
        wgt_update_counter <= _wgt_update_counter_T_1; // @[mult_loom.scala 171:28]
      end
    end
    if (io_reset) begin // @[mult_loom.scala 66:9]
      update_wgt <= 1'h0; // @[mult_loom.scala 69:30]
    end else begin
      update_wgt <= _T_2;
    end
    if (io_reset) begin // @[mult_loom.scala 66:9]
      update_wgt_1 <= 1'h0; // @[mult_loom.scala 70:30]
    end else begin
      update_wgt_1 <= update_wgt; // @[mult_loom.scala 98:22]
    end
    if (io_reset) begin // @[mult_loom.scala 66:9]
      update_wgt_2 <= 1'h0; // @[mult_loom.scala 71:30]
    end else begin
      update_wgt_2 <= update_wgt_1; // @[mult_loom.scala 99:22]
    end
    if (io_reset) begin // @[mult_loom.scala 66:9]
      update_wgt_3 <= 1'h0; // @[mult_loom.scala 72:30]
    end else begin
      update_wgt_3 <= update_wgt_2; // @[mult_loom.scala 100:22]
    end
    if (io_reset) begin // @[mult_loom.scala 66:9]
      update_wgt_4 <= 1'h0; // @[mult_loom.scala 73:30]
    end else begin
      update_wgt_4 <= update_wgt_3; // @[mult_loom.scala 101:22]
    end
    if (io_reset) begin // @[mult_loom.scala 66:9]
      update_wgt_5 <= 1'h0; // @[mult_loom.scala 74:30]
    end else begin
      update_wgt_5 <= update_wgt_4; // @[mult_loom.scala 102:22]
    end
    if (io_reset) begin // @[mult_loom.scala 66:9]
      update_wgt_6 <= 1'h0; // @[mult_loom.scala 75:30]
    end else begin
      update_wgt_6 <= update_wgt_5; // @[mult_loom.scala 103:22]
    end
    accum1_result <= _GEN_180[35:0];
    accum2_result <= _GEN_182[35:0];
    act_update_counter_6_r <= act_update_counter; // @[Reg.scala 16:19 Reg.scala 16:23 Reg.scala 15:16]
    act_update_counter_6_r_1 <= act_update_counter_6_r; // @[Reg.scala 16:19 Reg.scala 16:23 Reg.scala 15:16]
    act_update_counter_6_r_2 <= act_update_counter_6_r_1; // @[Reg.scala 16:19 Reg.scala 16:23 Reg.scala 15:16]
    act_update_counter_6_r_3 <= act_update_counter_6_r_2; // @[Reg.scala 16:19 Reg.scala 16:23 Reg.scala 15:16]
    act_update_counter_6_r_4 <= act_update_counter_6_r_3; // @[Reg.scala 16:19 Reg.scala 16:23 Reg.scala 15:16]
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  sw_length = _RAND_0[7:0];
  _RAND_1 = {1{`RANDOM}};
  activations_0 = _RAND_1[15:0];
  _RAND_2 = {1{`RANDOM}};
  activations_1 = _RAND_2[15:0];
  _RAND_3 = {1{`RANDOM}};
  activations_2 = _RAND_3[15:0];
  _RAND_4 = {1{`RANDOM}};
  activations_3 = _RAND_4[15:0];
  _RAND_5 = {1{`RANDOM}};
  activations_4 = _RAND_5[15:0];
  _RAND_6 = {1{`RANDOM}};
  activations_5 = _RAND_6[15:0];
  _RAND_7 = {1{`RANDOM}};
  activations_6 = _RAND_7[15:0];
  _RAND_8 = {1{`RANDOM}};
  activations_7 = _RAND_8[15:0];
  _RAND_9 = {1{`RANDOM}};
  activations_8 = _RAND_9[15:0];
  _RAND_10 = {1{`RANDOM}};
  activations_9 = _RAND_10[15:0];
  _RAND_11 = {1{`RANDOM}};
  activations_10 = _RAND_11[15:0];
  _RAND_12 = {1{`RANDOM}};
  activations_11 = _RAND_12[15:0];
  _RAND_13 = {1{`RANDOM}};
  activations_12 = _RAND_13[15:0];
  _RAND_14 = {1{`RANDOM}};
  activations_13 = _RAND_14[15:0];
  _RAND_15 = {1{`RANDOM}};
  activations_14 = _RAND_15[15:0];
  _RAND_16 = {1{`RANDOM}};
  activations_15 = _RAND_16[15:0];
  _RAND_17 = {1{`RANDOM}};
  weight_0 = _RAND_17[15:0];
  _RAND_18 = {1{`RANDOM}};
  weight_1 = _RAND_18[15:0];
  _RAND_19 = {1{`RANDOM}};
  weight_2 = _RAND_19[15:0];
  _RAND_20 = {1{`RANDOM}};
  weight_3 = _RAND_20[15:0];
  _RAND_21 = {1{`RANDOM}};
  weight_4 = _RAND_21[15:0];
  _RAND_22 = {1{`RANDOM}};
  weight_5 = _RAND_22[15:0];
  _RAND_23 = {1{`RANDOM}};
  weight_6 = _RAND_23[15:0];
  _RAND_24 = {1{`RANDOM}};
  weight_7 = _RAND_24[15:0];
  _RAND_25 = {1{`RANDOM}};
  weight_8 = _RAND_25[15:0];
  _RAND_26 = {1{`RANDOM}};
  weight_9 = _RAND_26[15:0];
  _RAND_27 = {1{`RANDOM}};
  weight_10 = _RAND_27[15:0];
  _RAND_28 = {1{`RANDOM}};
  weight_11 = _RAND_28[15:0];
  _RAND_29 = {1{`RANDOM}};
  weight_12 = _RAND_29[15:0];
  _RAND_30 = {1{`RANDOM}};
  weight_13 = _RAND_30[15:0];
  _RAND_31 = {1{`RANDOM}};
  weight_14 = _RAND_31[15:0];
  _RAND_32 = {1{`RANDOM}};
  weight_15 = _RAND_32[15:0];
  _RAND_33 = {1{`RANDOM}};
  init_activations_0 = _RAND_33[15:0];
  _RAND_34 = {1{`RANDOM}};
  init_activations_1 = _RAND_34[15:0];
  _RAND_35 = {1{`RANDOM}};
  init_activations_2 = _RAND_35[15:0];
  _RAND_36 = {1{`RANDOM}};
  init_activations_3 = _RAND_36[15:0];
  _RAND_37 = {1{`RANDOM}};
  init_activations_4 = _RAND_37[15:0];
  _RAND_38 = {1{`RANDOM}};
  init_activations_5 = _RAND_38[15:0];
  _RAND_39 = {1{`RANDOM}};
  init_activations_6 = _RAND_39[15:0];
  _RAND_40 = {1{`RANDOM}};
  init_activations_7 = _RAND_40[15:0];
  _RAND_41 = {1{`RANDOM}};
  init_activations_8 = _RAND_41[15:0];
  _RAND_42 = {1{`RANDOM}};
  init_activations_9 = _RAND_42[15:0];
  _RAND_43 = {1{`RANDOM}};
  init_activations_10 = _RAND_43[15:0];
  _RAND_44 = {1{`RANDOM}};
  init_activations_11 = _RAND_44[15:0];
  _RAND_45 = {1{`RANDOM}};
  init_activations_12 = _RAND_45[15:0];
  _RAND_46 = {1{`RANDOM}};
  init_activations_13 = _RAND_46[15:0];
  _RAND_47 = {1{`RANDOM}};
  init_activations_14 = _RAND_47[15:0];
  _RAND_48 = {1{`RANDOM}};
  init_activations_15 = _RAND_48[15:0];
  _RAND_49 = {1{`RANDOM}};
  ser_act_0 = _RAND_49[0:0];
  _RAND_50 = {1{`RANDOM}};
  ser_act_1 = _RAND_50[0:0];
  _RAND_51 = {1{`RANDOM}};
  ser_act_2 = _RAND_51[0:0];
  _RAND_52 = {1{`RANDOM}};
  ser_act_3 = _RAND_52[0:0];
  _RAND_53 = {1{`RANDOM}};
  ser_act_4 = _RAND_53[0:0];
  _RAND_54 = {1{`RANDOM}};
  ser_act_5 = _RAND_54[0:0];
  _RAND_55 = {1{`RANDOM}};
  ser_act_6 = _RAND_55[0:0];
  _RAND_56 = {1{`RANDOM}};
  ser_act_7 = _RAND_56[0:0];
  _RAND_57 = {1{`RANDOM}};
  ser_act_8 = _RAND_57[0:0];
  _RAND_58 = {1{`RANDOM}};
  ser_act_9 = _RAND_58[0:0];
  _RAND_59 = {1{`RANDOM}};
  ser_act_10 = _RAND_59[0:0];
  _RAND_60 = {1{`RANDOM}};
  ser_act_11 = _RAND_60[0:0];
  _RAND_61 = {1{`RANDOM}};
  ser_act_12 = _RAND_61[0:0];
  _RAND_62 = {1{`RANDOM}};
  ser_act_13 = _RAND_62[0:0];
  _RAND_63 = {1{`RANDOM}};
  ser_act_14 = _RAND_63[0:0];
  _RAND_64 = {1{`RANDOM}};
  ser_act_15 = _RAND_64[0:0];
  _RAND_65 = {1{`RANDOM}};
  ser_weight_0 = _RAND_65[0:0];
  _RAND_66 = {1{`RANDOM}};
  ser_weight_1 = _RAND_66[0:0];
  _RAND_67 = {1{`RANDOM}};
  ser_weight_2 = _RAND_67[0:0];
  _RAND_68 = {1{`RANDOM}};
  ser_weight_3 = _RAND_68[0:0];
  _RAND_69 = {1{`RANDOM}};
  ser_weight_4 = _RAND_69[0:0];
  _RAND_70 = {1{`RANDOM}};
  ser_weight_5 = _RAND_70[0:0];
  _RAND_71 = {1{`RANDOM}};
  ser_weight_6 = _RAND_71[0:0];
  _RAND_72 = {1{`RANDOM}};
  ser_weight_7 = _RAND_72[0:0];
  _RAND_73 = {1{`RANDOM}};
  ser_weight_8 = _RAND_73[0:0];
  _RAND_74 = {1{`RANDOM}};
  ser_weight_9 = _RAND_74[0:0];
  _RAND_75 = {1{`RANDOM}};
  ser_weight_10 = _RAND_75[0:0];
  _RAND_76 = {1{`RANDOM}};
  ser_weight_11 = _RAND_76[0:0];
  _RAND_77 = {1{`RANDOM}};
  ser_weight_12 = _RAND_77[0:0];
  _RAND_78 = {1{`RANDOM}};
  ser_weight_13 = _RAND_78[0:0];
  _RAND_79 = {1{`RANDOM}};
  ser_weight_14 = _RAND_79[0:0];
  _RAND_80 = {1{`RANDOM}};
  ser_weight_15 = _RAND_80[0:0];
  _RAND_81 = {1{`RANDOM}};
  and_result_0 = _RAND_81[0:0];
  _RAND_82 = {1{`RANDOM}};
  and_result_1 = _RAND_82[0:0];
  _RAND_83 = {1{`RANDOM}};
  and_result_2 = _RAND_83[0:0];
  _RAND_84 = {1{`RANDOM}};
  and_result_3 = _RAND_84[0:0];
  _RAND_85 = {1{`RANDOM}};
  and_result_4 = _RAND_85[0:0];
  _RAND_86 = {1{`RANDOM}};
  and_result_5 = _RAND_86[0:0];
  _RAND_87 = {1{`RANDOM}};
  and_result_6 = _RAND_87[0:0];
  _RAND_88 = {1{`RANDOM}};
  and_result_7 = _RAND_88[0:0];
  _RAND_89 = {1{`RANDOM}};
  and_result_8 = _RAND_89[0:0];
  _RAND_90 = {1{`RANDOM}};
  and_result_9 = _RAND_90[0:0];
  _RAND_91 = {1{`RANDOM}};
  and_result_10 = _RAND_91[0:0];
  _RAND_92 = {1{`RANDOM}};
  and_result_11 = _RAND_92[0:0];
  _RAND_93 = {1{`RANDOM}};
  and_result_12 = _RAND_93[0:0];
  _RAND_94 = {1{`RANDOM}};
  and_result_13 = _RAND_94[0:0];
  _RAND_95 = {1{`RANDOM}};
  and_result_14 = _RAND_95[0:0];
  _RAND_96 = {1{`RANDOM}};
  and_result_15 = _RAND_96[0:0];
  _RAND_97 = {1{`RANDOM}};
  sum_result = _RAND_97[15:0];
  _RAND_98 = {1{`RANDOM}};
  act_update_counter = _RAND_98[7:0];
  _RAND_99 = {1{`RANDOM}};
  act_update_counter_6 = _RAND_99[7:0];
  _RAND_100 = {1{`RANDOM}};
  wgt_update_counter = _RAND_100[7:0];
  _RAND_101 = {1{`RANDOM}};
  update_wgt = _RAND_101[0:0];
  _RAND_102 = {1{`RANDOM}};
  update_wgt_1 = _RAND_102[0:0];
  _RAND_103 = {1{`RANDOM}};
  update_wgt_2 = _RAND_103[0:0];
  _RAND_104 = {1{`RANDOM}};
  update_wgt_3 = _RAND_104[0:0];
  _RAND_105 = {1{`RANDOM}};
  update_wgt_4 = _RAND_105[0:0];
  _RAND_106 = {1{`RANDOM}};
  update_wgt_5 = _RAND_106[0:0];
  _RAND_107 = {1{`RANDOM}};
  update_wgt_6 = _RAND_107[0:0];
  _RAND_108 = {2{`RANDOM}};
  accum1_result = _RAND_108[35:0];
  _RAND_109 = {2{`RANDOM}};
  accum2_result = _RAND_109[35:0];
  _RAND_110 = {1{`RANDOM}};
  act_update_counter_6_r = _RAND_110[7:0];
  _RAND_111 = {1{`RANDOM}};
  act_update_counter_6_r_1 = _RAND_111[7:0];
  _RAND_112 = {1{`RANDOM}};
  act_update_counter_6_r_2 = _RAND_112[7:0];
  _RAND_113 = {1{`RANDOM}};
  act_update_counter_6_r_3 = _RAND_113[7:0];
  _RAND_114 = {1{`RANDOM}};
  act_update_counter_6_r_4 = _RAND_114[7:0];
`endif // RANDOMIZE_REG_INIT
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
