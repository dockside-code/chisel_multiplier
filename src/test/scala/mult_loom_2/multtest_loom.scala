// See README.md for license details.

package mult_loom_2

import chisel3._
import chisel3.util._
import chisel3.tester._
import org.scalatest.FreeSpec
import scala.util._



class multtest_loom_2 extends FreeSpec with ChiselScalatestTester {
  //val result = 0xABABCD.U * 0xABABCD.U
  "loom should multiply random numbers" in {
  test(new mult_loom_2(16, 16)) { c =>
  def product_16(a: Long, b:Long): Long = {
    a * b
  }
  def s_product_16(a: Short, b:Short): Int = {
    a * b
  }

  val random = new Random()
  val testvalues1 = Seq.fill(16)(Random.nextInt(0x1ABC))
  val testvalues2 = Seq.fill(16)(Random.nextInt(0x1ABC))

  val mult_answer = (testvalues1, testvalues2).zipped.map(_ * _)
  val sum_answer = mult_answer.sum
  
  /*val random = new Random()
  val testval1 = random.nextInt(0xFFEB)
  val testval2 = random.nextInt(0xFFEB)*/
  //val testval1 = 0xABCD
  //val testval2 = 0xDCBA
  //val test_vec_act = Vec(testval1, testval2)
  //val test_vec_wgt = Vec(testval1, testval2)
  /*for {
    i <- testval1 to (testval1 + 20)
    j <- testval2 to (testval2 + 20)
  }*/

  for(i <- 0 to 15)
  {
  c.io.activations(i).poke(testvalues1(i).U)
  c.io.weight(i).poke(testvalues2(i).U)
  //println(s"Print during testing: result is ${i} * ${j - 7} = ${c.io.outputProduct.peek()}")
  }

  /*c.io.activations(0).poke(testval1.U)
  c.io.activations(1).poke(testval2.U)
  c.io.activations(2).poke(testval1.U)
  c.io.activations(3).poke(testval2.U)
  c.io.activations(4).poke(testval1.U)
  c.io.activations(5).poke(testval2.U)
  c.io.weight(0).poke(testval1.U)
  c.io.weight(1).poke(testval2.U)
  c.io.weight(2).poke(testval1.U)
  c.io.weight(3).poke(testval2.U)
  c.io.weight(4).poke(testval1.U)
  c.io.weight(5).poke(testval2.U)*/
  c.io.sw_length.poke(16.U)
  c.io.loadingValues.poke(true.B)
  c.clock.step(1)
  c.io.loadingValues.poke(false.B)
  c.clock.step(1)        //bits + 4                       
  //c.io.loadingValues.poke(false.B)
  c.clock.step(300)
  c.io.outputProduct.expect(sum_answer.U) 
  /*c.io.value1.poke((0xABCD).U)
  c.io.value2.poke((0xABCD).U)
  c.io.sw_length.poke(4.U)
  c.io.signed.poke(1.U)
  c.io.loadingValues.poke(true.B)
  c.clock.step(8)*/
   
}

//println("TESTING: SUCCESS!!")

  }
}