// See README.md for license details.

package mult_loom

import chisel3._
import chisel3.util._
import chisel3.tester._
import org.scalatest.FreeSpec
import scala.util._



class multtest_loom extends FreeSpec with ChiselScalatestTester {
  //val result = 0xABABCD.U * 0xABABCD.U
  "loom should multiply random numbers" in {
  test(new mult_loom(2, 16)) { c =>
  def product_16(a: Long, b:Long): Long = {
    a * b
  }
  def s_product_16(a: Short, b:Short): Int = {
    a * b
  }

  val random = new Random()
  /*val testvalues1 = Seq.fill(16)(Random.nextInt(0xFFF))
  val testvalues2 = Seq.fill(16)(Random.nextInt(0xFFF))
  val testvalues3 = Seq.fill(16)(Random.nextInt(0xFF))
  val testvalues4 = Seq.fill(16)(Random.nextInt(0xFF))

  val mult_answer_1 = (testvalues1, testvalues2).zipped.map(_ * _)
  val sum_answer_1 = mult_answer_1.sum
  val mult_answer_2 = (testvalues3, testvalues4).zipped.map(_ * _)
  val sum_answer_2 = mult_answer_2.sum

  for(i <- 0 to 15)
  {
  c.io.activations(i).poke(testvalues1(i).U)
  c.io.weight(i).poke(testvalues2(i).U)
  
  }*/
  c.io.activations(0).poke(0xABC.U)
  c.io.weight(0).poke(0xABC.U)
  c.io.activations(1).poke(0xABC.U)
  c.io.weight(1).poke(0xABC.U)

  c.io.sw_length.poke(12.U)
  c.io.loadingValues.poke(true.B)
  c.clock.step(1)
  c.io.loadingValues.poke(false.B)
  c.clock.step(1)        //bits + 4                       
  //c.io.loadingValues.poke(false.B)
  c.clock.step(147)
  //c.io.outputProduct.expect(sum_answer_1.U)
  c.io.outputProduct.expect(0xE67420.U)
  

  /*for(i <- 0 to 15)
  {
  c.io.activations(i).poke(testvalues3(i).U)
  c.io.weight(i).poke(testvalues4(i).U)
  //println(s"Print during testing: result is ${i} * ${j - 7} = ${c.io.outputProduct.peek()}")
  }
  c.io.sw_length.poke(8.U)
  c.io.loadingValues.poke(true.B)
  c.clock.step(1)
  c.io.loadingValues.poke(false.B)
  c.clock.step(1)        //bits + 4                       
  //c.io.loadingValues.poke(false.B)
  c.clock.step(67)
  c.io.outputProduct.expect(sum_answer_2.U)*/ 
   
}

//println("TESTING: SUCCESS!!")

  }
}