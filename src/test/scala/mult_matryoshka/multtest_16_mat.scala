// See README.md for license details.

package mult_16_mat

import chisel3._
import chisel3.util._
import chisel3.tester._
import org.scalatest.FreeSpec



class multtest_16_mat extends FreeSpec with ChiselScalatestTester {
  //val result = 0xABABCD.U * 0xABABCD.U
  "multiplier_16 should multiply 16bits" in {
  test(new mult_16_mat(16)) { c =>
  c.io.value1.poke(0xABCD.U)
  c.io.value2.poke(0xABCD.U)
  c.io.sw_32.poke(false.B)
  c.io.sw_16.poke(true.B)
  c.io.sw_8.poke(false.B)
  c.io.sw_4.poke(false.B)
  c.io.sw_2.poke(false.B)
  c.io.signed.poke(0.U)
  c.io.loadingValues.poke(true.B)
  c.clock.step(1)
  /*c.io.value1.poke(0xDCBABA.U)
  c.io.value2.poke(0xDCBABA.U)
  c.io.sw_32.poke(true.B)
  c.io.sw_16.poke(false.B)
  c.io.sw_8.poke(false.B)
  c.io.sw_4.poke(false.B)
  c.io.sw_2.poke(false.B)
  c.io.signed.poke(1.U)
  c.io.loadingValues.poke(true.B)
  c.clock.step(1)        //bits + 4*/
    
  c.clock.step(8)        //bits + 4
}

//println("TESTING: SUCCESS!!")

  }
}