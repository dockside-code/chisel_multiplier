// See README.md for license details.

package mult_iter

import chisel3._
import chisel3.util._
import chisel3.tester._
import org.scalatest.FreeSpec
import scala.util._



class multtest_iter extends FreeSpec with ChiselScalatestTester {
  //val result = 0xABABCD.U * 0xABABCD.U
  "multiplier should multiply random numbers" in {
  test(new mult_iter(16)) { c =>
  def product_16(a: Long, b:Long): Long = {
    a * b
  }
  def s_product_16(a: Short, b:Short): Int = {
    a * b
  }
  
  val random = new Random()
  val testval1 = random.nextInt(0xFFEB)
  val testval2 = random.nextInt(0xFFEB)
  for {
    i <- testval1 to (testval1 + 20)
    j <- testval2 to (testval2 + 20)
  }

  {
  c.io.value1.poke((i).U)
  c.io.value2.poke((j).U)
  c.io.sw_length.poke(16.U)
  c.io.signed.poke(0.U)
  c.io.loadingValues.poke(true.B)
  /*c.clock.step(1)
  c.io.loadingValues.poke(false.B)*/
  c.clock.step(1)        //bits + 4                       
  if(j >= (testval2 + 7))
  {
    c.io.outputProduct.expect(product_16((i), (j - 7)).U)
    //println(s"Print during testing: result is ${i} * ${j - 7} = ${c.io.outputProduct.peek()}")
  }
  }
  /*c.io.value1.poke((0xABCD).U)
  c.io.value2.poke((0xABCD).U)
  c.io.sw_length.poke(4.U)
  c.io.signed.poke(1.U)
  c.io.loadingValues.poke(true.B)
  c.clock.step(8)*/
   
}

//println("TESTING: SUCCESS!!")

  }
}