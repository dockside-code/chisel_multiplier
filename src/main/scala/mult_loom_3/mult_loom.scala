// See README.md for license details.
package mult_loom_3
import chisel3._
import chisel3.util._
import scala.math._



/**
  * Multiplier of serial logic
  * supports 16 bit inputs
  * returns 32 bit results
  */
  
  
class mult_loom_3 (LaneNum: Int, NumWidth: Int) extends Module {
  val io = IO(new Bundle {
    val activations   = Input(Vec(LaneNum, UInt(NumWidth.W)))
	val weight        = Input(Vec(LaneNum, UInt(NumWidth.W)))
	val sw_length	  = Input(UInt(8.W))
	val reset		  = Input(Bool())
	val loadingValues = Input(Bool())
    val outputProduct = Output(UInt((NumWidth * 2).W))
  })
  
  val sw_length	 		= Reg(UInt(8.W))
  val activations   	= Reg(Vec(LaneNum, UInt(NumWidth.W)))
  
  val weight        	= Reg(Vec(LaneNum, UInt(NumWidth.W)))
  val init_activations	= Reg(Vec(LaneNum, UInt(NumWidth.W)))
  val init_weight       = Reg(Vec(LaneNum, UInt(NumWidth.W)))
  val ser_act	  		= Reg(Vec(LaneNum, UInt(1.W)))
  val ser_weight  		= Reg(Vec(LaneNum, UInt(1.W)))
  val and_result 		= Reg(Vec(LaneNum, UInt(1.W)))
  val sum_result  		= Reg(UInt(LaneNum.W))

  val act_update_counter = Reg(UInt(8.W))
  //val act_update = Reg(UInt(NumWidth.W))
  val act_update_counter_6 = Reg(UInt(8.W))
  val wgt_update_counter = Reg(UInt(8.W))

  val update_wgt	 = Reg(Bool())
  val update_wgt_1	 = Reg(Bool())
  val update_wgt_2	 = Reg(Bool())
  val update_wgt_3	 = Reg(Bool())
  val update_wgt_4	 = Reg(Bool())
  val update_wgt_5	 = Reg(Bool())
  val update_wgt_6	 = Reg(Bool())
  val update_act 	 = Reg(Bool())


  val accum1_result  	= Reg(UInt((NumWidth * 2 + 4).W))
  val accum2_result  	= Reg(UInt((NumWidth * 2 + 4).W))
  //val result = Wire(Vec(dwordwidth, UInt(1.W)))
  
    
    when(io.loadingValues) {
    activations := io.activations
	init_activations := io.activations
	init_weight := io.weight
	sw_length := io.sw_length										
    weight := io.weight
  }
    
	when(io.reset)
	{
		//update_act := false.B
		act_update_counter := 0.U							//reset states after each full operation
		update_wgt   := false.B
		update_wgt_1 := false.B
		update_wgt_2 := false.B
		update_wgt_3 := false.B
		update_wgt_4 := false.B
		update_wgt_5 := false.B
		update_wgt_6 := false.B
		for(which_lane <- 0 until LaneNum)
		{
			activations(which_lane) := 0.U
			weight(which_lane) := 0.U
		}
		//act_update := 0.U
		act_update_counter_6 := 0.U
		accum1_result := 0.U
		accum2_result := 0.U
	}
	.otherwise
	{
	when(act_update_counter === (sw_length - 1.U))
	{
		act_update_counter := 0.U
		update_wgt := true.B
	}
	.otherwise
	{
		act_update_counter := act_update_counter + 1.U
		update_wgt := false.B
	}
	update_wgt_1 := update_wgt
	update_wgt_2 := update_wgt_1
	update_wgt_3 := update_wgt_2
	update_wgt_4 := update_wgt_3												//counter and corresponding control signals
	update_wgt_5 := update_wgt_4												//to regulate value updates
	update_wgt_6 := update_wgt_5
	act_update_counter_6 := ShiftRegister(act_update_counter, 5)
	}
	/*.otherwise
	{
		act_update_counter := 0.U
		update_wgt   := false.B
		update_wgt_1 := false.B
		update_wgt_2 := false.B
		update_wgt_3 := false.B
		update_wgt_4 := false.B
		update_wgt_5 := false.B
		update_wgt_6 := false.B
		act_update_counter_6 := 0.U
	}*/

  	for (which_weight_lane <- 0 until LaneNum)
 	{
	
	val weight_update = weight(which_weight_lane) >> 1								

	ser_weight(which_weight_lane) := weight(which_weight_lane)(0).asUInt			//for each weight lane bit: shift right after use
	when(update_wgt_2)
	{
	weight(which_weight_lane) := weight_update
	}
	}

	for (which_act_lane <- 0 until LaneNum)
	{
	val act_update = Cat(activations(which_act_lane)(0), activations(which_act_lane) >> 1)
	ser_act(which_act_lane) := activations(which_act_lane)(0).asUInt			//for each activation lane bit: shift right after use
	when(activations(which_act_lane) =/= 0.U)
	{
	when(io.reset)
	{activations(which_act_lane) := 0.U}										//implemented a rotate circular shift register that can
	.otherwise																	//reset its values, and to zero if reset is true
	{
	when(update_wgt_2)
	{activations(which_act_lane) := init_activations(which_act_lane)}
	.otherwise
	{activations(which_act_lane) := act_update}
	}
	}
	//printf("Print during simulation: act is %x, %x\n", activations(which_act_lane), act_update)
	}
	
	//printf("act, wgt is %x, %x, becuz %x\n", ser_act.asUInt, ser_weight.asUInt, update_wgt_2.asUInt)

	for (which_lane <- 0 until LaneNum)
	{
	and_result(which_lane) := ser_act(which_lane) & ser_weight(which_lane)
	}
	val vec_and_result = VecInit.tabulate(LaneNum)(i => and_result(i))			//and gates and adders implemented in parallel
	sum_result := vec_and_result.reduce(_ +& _)
	//printf("sum_result is %x\n", sum_result)

	when(update_wgt_6)
	{
	accum1_result := (sum_result)												//accum_1 clears after each weight update

	when(wgt_update_counter === (sw_length - 1.U))								//accumulator operations: accum_1 stores partial
	{																			//results for each bit, accum_2 stores full result
	wgt_update_counter := 0.U
	//accum2_result := (accum1_result << wgt_update_counter)
	}
	.otherwise
	{
	wgt_update_counter := wgt_update_counter + 1.U
	//accum2_result := (accum1_result << wgt_update_counter) + accum2_result
	}
	accum2_result := (accum1_result << wgt_update_counter) + accum2_result
		
	}
	.otherwise
	{
		accum1_result := ((sum_result << act_update_counter_6) + accum1_result)
	}
	
	io.outputProduct := accum2_result
	//printf("Print during simulation: accum1(%d), accum2(%d) is %x, %x, %x\n"
	//, act_update_counter_6, wgt_update_counter, accum1_result.asUInt, accum2_result.asUInt, update_wgt_6.asUInt )
}

import chisel3.stage.ChiselStage

object MultiplierDriver extends App {
	(new ChiselStage).emitVerilog(new mult_loom_3(16, 16))
}
