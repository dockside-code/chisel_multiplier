// See README.md for license details.
package mult_loom
import chisel3._
import chisel3.util._
import scala.math._



/**
  * Multiplier of serial logic
  * supports 16 bit inputs
  * returns 32 bit results
  */
  
  
class mult_loom (LaneNum: Int, NumWidth: Int) extends Module {
  val io = IO(new Bundle {
    val activations   = Input(Vec(LaneNum, UInt(NumWidth.W)))
	val weight        = Input(Vec(LaneNum, UInt(NumWidth.W)))
	val sw_length	  = Input(UInt(8.W))
	val loadingValues = Input(Bool())
    val outputProduct = Output(UInt((NumWidth * 2).W))
  })
  
  val sw_length	 		= Reg(UInt(8.W))
  val activations   	= Reg(Vec(LaneNum, UInt(NumWidth.W)))
  
  val weight        	= Reg(Vec(LaneNum, UInt(NumWidth.W)))
  val init_activations	= Reg(Vec(LaneNum, UInt(NumWidth.W)))
  val init_weight       = Reg(Vec(LaneNum, UInt(NumWidth.W)))
  val ser_act	  		= Reg(Vec(LaneNum, UInt(1.W)))
  val ser_weight  		= Reg(Vec(LaneNum, UInt(1.W)))
  val and_result 		= Reg(Vec(LaneNum, UInt(1.W)))
  val sum_result  		= Reg(UInt(LaneNum.W))

  val act_update_counter = Reg(UInt(8.W))
  val wgt_update_counter = Reg(UInt(8.W))

  val accum1_result  	= Reg(UInt((NumWidth * 2 + 4).W))
  val accum2_result  	= Reg(UInt((NumWidth * 2 + 4).W))
  //val result = Wire(Vec(dwordwidth, UInt(1.W)))
  
    
    when(io.loadingValues) {
    activations := io.activations
	init_activations := io.activations
	init_weight := io.weight
	sw_length := io.sw_length										
    weight := io.weight
  }

	when(act_update_counter === (sw_length - 1.U))
	{
		act_update_counter := 0.U
	}
	.otherwise
	{
		act_update_counter := act_update_counter + 1.U
	}

	val act_update_counter_2 = RegNext(act_update_counter)
	val act_update_counter_3 = RegNext(act_update_counter_2)
	val act_update_counter_4 = RegNext(act_update_counter_3)
	val act_update_counter_5 = RegNext(act_update_counter_4)
	val act_update_counter_6 = RegNext(act_update_counter_5)
	val act_update_counter_7 = RegNext(act_update_counter_6)

  	for (which_weight_lane <- 0 until LaneNum)
 	{
	//printf("Print during simulation: weight is %d, %x\n", which_weight_lane.U, weight(which_weight_lane) )
	
	val weight_update = weight(which_weight_lane) >> 1								//!!!!!!!!

	ser_weight(which_weight_lane) := weight(which_weight_lane)(0).asUInt			//for each weight lane bit: shift right after use
	when(act_update_counter_3 === (sw_length - 1.U))
	{
	weight(which_weight_lane) := weight_update
	}
	}

	for (which_act_lane <- 0 until LaneNum)
	{
	
	val act_update = Cat(activations(which_act_lane)(0), activations(which_act_lane) >> 1)
	ser_act(which_act_lane) := activations(which_act_lane)(0).asUInt			//for each activation lane bit: shift right after use
	when(activations(which_act_lane) =/= 0.U)
	{
	when(act_update_counter_3 === (sw_length - 1.U))
	{activations(which_act_lane) := init_activations(which_act_lane)}
	.otherwise
	{activations(which_act_lane) := act_update}
	}
	//printf("Print during simulation: act is %x\n", activations(which_act_lane))
	}
	

	for (which_lane <- 0 until LaneNum)
	{
	and_result(which_lane) := ser_act(which_lane) & ser_weight(which_lane)
	//printf("and(%d) is %x & %x = %x\n", which_lane.U, ser_act(which_lane), ser_weight(which_lane), and_result(which_lane))

	}
	val vec_and_result = VecInit.tabulate(LaneNum)(i => and_result(i))
	sum_result := vec_and_result.reduce(_ +& _)
	//printf("Print during simulation: sum_result(%d) of %x, %x is %x\n", act_update_counter_4, ser_act.asUInt, 
	//ser_weight.asUInt, sum_result )
	when(act_update_counter_7 === (sw_length - 1.U))
	{
		accum1_result := (sum_result)
	}
	.otherwise
	{
		accum1_result := ((sum_result << act_update_counter_6) + accum1_result)
	}

	when(act_update_counter_7 === (sw_length - 1.U))
	{
	when(wgt_update_counter === (sw_length - 1.U))
	{
	wgt_update_counter := 0.U
	//accum2_result := (accum1_result << wgt_update_counter)
	}
	.otherwise
	{
	wgt_update_counter := wgt_update_counter + 1.U
	//accum2_result := (accum1_result << wgt_update_counter) + accum2_result
	}
	accum2_result := (accum1_result << wgt_update_counter) + accum2_result
	}

	when(act_update_counter_7 === 0.U)
	{
	when(wgt_update_counter === 0.U)
	{accum2_result := 0.U}
	}
	
 

	io.outputProduct := accum2_result
	//printf("Print during simulation: accum1(%d), accum2(%d) is %x, %x\n"
	//, act_update_counter_7, wgt_update_counter, accum1_result.asUInt, accum2_result.asUInt )
}
