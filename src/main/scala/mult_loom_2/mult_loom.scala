// See README.md for license details.
package mult_loom_2
import chisel3._
import chisel3.util._
import scala.math._



/**
  * Multiplier of serial logic
  * supports 16 bit inputs
  * returns 32 bit results
  */
  
  
class mult_loom_2 (LaneNum: Int, NumWidth: Int) extends Module {
  val io = IO(new Bundle {
    val activations   = Input(Vec(LaneNum, UInt(NumWidth.W)))
	val weight        = Input(Vec(LaneNum, UInt(NumWidth.W)))
	val sw_length	  = Input(UInt(8.W))
	val loadingValues = Input(Bool())
    val outputProduct = Output(UInt((NumWidth * 2).W))
  })
  
  val sw_length	 		= Reg(UInt(8.W))
  val activations   	= Reg(Vec(LaneNum, UInt(NumWidth.W)))
  //val init_activations	= Reg(Vec(LaneNum, UInt(NumWidth.W)))
  val weight        	= Reg(Vec(LaneNum, UInt(NumWidth.W)))
  val ser_act	  		= Reg(Vec(LaneNum, UInt(1.W)))
  val ser_weight  		= Reg(Vec(LaneNum, UInt(1.W)))
  val and_result 		= Reg(Vec(LaneNum, UInt(1.W)))
  val mod_and_result 		= Reg(Vec(LaneNum, UInt(1.W)))
  val sum_result  		= Reg(UInt(LaneNum.W))

  val act_update_counter = Reg(UInt(8.W))
  val wgt_update_counter = Reg(UInt(8.W))
  val act_swd_update_counter = Reg(UInt(8.W))
  val wgt_swd_update_counter = Reg(UInt(8.W))
  val act_swd = Reg(UInt(3.W))
  val wgt_swd = Reg(UInt(3.W))

  val accum1_result  	= Reg(UInt((NumWidth * 2 + 4).W))
  val accum2_result  	= Reg(UInt((NumWidth * 2 + 4).W))
  //val result = Wire(Vec(dwordwidth, UInt(1.W)))
  
    
    when(io.loadingValues) {
    activations := io.activations
	sw_length := io.sw_length										
    weight := io.weight
  }
	when(act_update_counter === ((NumWidth - 1).U))						//counter for indicating which activation bit this is
	{
		act_update_counter := 0.U
	}
	.otherwise
	{
		act_update_counter := act_update_counter + 1.U
	}

	when(act_swd_update_counter === sw_length - 1.U)
	{
		act_swd_update_counter := 0.U									//swd_counter loops over subword length to faciitate act_swd
		//act_swd := act_swd + 1.U
	}
	.otherwise
	{
		act_swd_update_counter := act_swd_update_counter + 1.U
	}

	when(act_update_counter === ((NumWidth - 1).U))
	{
		act_swd := 0.U													//act_swd indicates which activation subword this is
	}
	.otherwise
	{
		when(act_swd_update_counter === sw_length - 1.U)
		{act_swd := act_swd + 1.U}
	}

	val act_update_counter_2 = RegNext(act_update_counter)
	val act_update_counter_3 = RegNext(act_update_counter_2)
	val act_update_counter_4 = RegNext(act_update_counter_3)
	val act_update_counter_5 = RegNext(act_update_counter_4)
	val act_update_counter_6 = RegNext(act_update_counter_5)
	val act_update_counter_7 = RegNext(act_update_counter_6)
	val act_update_counter_8 = RegNext(act_update_counter_7)

	val act_swd_2 = RegNext(act_swd)
	val act_swd_3 = RegNext(act_swd_2)
	val act_swd_4 = RegNext(act_swd_3)
	val act_swd_5 = RegNext(act_swd_4)
	val act_swd_6 = RegNext(act_swd_5)


  	for (which_weight_lane <- 0 until LaneNum)
 	{
	val weight_update = weight(which_weight_lane) >> 1
	ser_weight(which_weight_lane) := weight(which_weight_lane)(0).asUInt			//for each weight lane bit: shift right after use
	when(act_update_counter_3 === (NumWidth - 1).U)
	{
	weight(which_weight_lane) := weight_update
	}
	}
	
	//printf("Print during simulation: ser_weight is %x\n", ser_weight.asUInt)

	for (which_act_lane <- 0 until LaneNum)
	{
	
	val act_update = Cat(activations(which_act_lane)(0), activations(which_act_lane) >> 1)
	ser_act(which_act_lane) := activations(which_act_lane)(0).asUInt
	/*when(act_update_counter_3 === (NumWidth - 1).U)
	{			
	activations(which_act_lane) := init_activations(which_act_lane)			//for each activation lane bit: shift right after use
	}*/
	when(activations(which_act_lane) =/= 0.U)
	{
	activations(which_act_lane) := act_update
	}
	//printf("Print during simulation: act is %x\n", activations(which_act_lane))
	}
	

	for (which_lane <- 0 until LaneNum)
	{
	and_result(which_lane) := ser_act(which_lane) & ser_weight(which_lane)	//for each lane: calculate and results
	//printf("and(%d) is %x\n", which_lane.U, and_result(which_lane))
	}

	//printf("act_swd is %d, wgt_swd is %d, and rslt is %d\n", act_swd_5, wgt_swd, and_result.asUInt)
	when(act_swd_5 === wgt_swd)
	{
		for (which_lane <- 0 until LaneNum)
		{
			mod_and_result(which_lane) := and_result(which_lane)			//if they are not in same subword region: set to 0
		}																	//to support subword multiplications
	}
	.otherwise
	{
		for (which_lane <- 0 until LaneNum)
		{
			mod_and_result(which_lane) := 0.U
		}
	}

	val vec_and_result = VecInit.tabulate(LaneNum)(i => mod_and_result(i))	//sum them up
	sum_result := vec_and_result.reduce(_ +& _)
	//printf("Print during simulation: sum_result(%d) of %x, %x is %x\n", act_update_counter_4, ser_act.asUInt, 
	//ser_weight.asUInt, sum_result )

	when(act_update_counter_5 === (NumWidth - 1).U)
	{
	when(wgt_update_counter === (NumWidth - 1).U)
	{
	wgt_update_counter := 0.U												//wgt_update_counter used to indicate which 
	}																		//weight bit we are processing
	.otherwise
	{
	wgt_update_counter := wgt_update_counter + 1.U
	}

	when(wgt_swd_update_counter === (sw_length - 1.U))
	{
	wgt_swd_update_counter := 0.U											//loops over subword, facilitates wgt_swd
	//wgt_swd := wgt_swd + 1.U
	}
	.otherwise
	{
	wgt_swd_update_counter := wgt_swd_update_counter + 1.U
	}

	when(wgt_update_counter === (NumWidth - 1).U)
	{
		wgt_swd := 0.U
	}
	.otherwise
	{
		when(wgt_swd_update_counter === (sw_length - 1.U))
		{
			wgt_swd := wgt_swd + 1.U										//indicates which weight subword this is
		}
	}
	}

	val wgt_update_counter_2 = RegNext(wgt_update_counter)
	val wgt_update_counter_3 = RegNext(wgt_update_counter_2)
	val wgt_update_counter_4 = RegNext(wgt_update_counter_3)
	//val wgt_update_counter_5 = RegNext(wgt_update_counter_4)

	when(act_update_counter_8 === (NumWidth - 1).U)
	{

	accum1_result := (sum_result)											//end of cycle: shift and send to accumulator2
	accum2_result := (accum1_result << wgt_update_counter_4) + accum2_result//accum2 stores accumresult for all prev. bits
	}
	.otherwise
	{																		//accum1 stores subresult for current bit 
		accum1_result := ((sum_result << act_update_counter_7) + accum1_result)
	}
 

	io.outputProduct := accum2_result
	//printf("act_swd: %d, wgt_swd: %d\n", act_swd_5, wgt_swd)
	//printf("weight subword is %d, activation subword is %d\n", wgt_swd, act_swd)
	//printf("Print during simulation: accum1(%d), accum2(%d) is %x, %x\n"
	//, act_update_counter_7, wgt_update_counter_4, accum1_result.asUInt, accum2_result.asUInt )
}