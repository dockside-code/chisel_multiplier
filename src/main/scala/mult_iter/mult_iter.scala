// See README.md for license details.
package mult_iter
import chisel3._
import chisel3.util._
import scala.math._



/**
  * Multiplier of combinational logic
  * supports 16 bit inputs
  * returns 32 bit results
  */
  
  
class mult_iter (NumWidth: Int) extends Module {
  val io = IO(new Bundle {
    val value1        = Input(UInt(NumWidth.W))
    val value2        = Input(UInt(NumWidth.W))
	val sw_length	  = Input(UInt((((log10(NumWidth)/log10(2)).toInt) + 1).W))
	val signed		  = Input(UInt(1.W))
	val loadingValues = Input(Bool())
    val outputProduct = Output(UInt((NumWidth * 2).W))
  })
  val dwordwidth = NumWidth * 2
  val log2_of_width = (log10(NumWidth)/log10(2)).toInt
  val multi  	 = Reg(Vec(NumWidth, Vec(NumWidth, UInt(1.W))))
  val sign_flip  = Reg(Vec(NumWidth, Vec((NumWidth + 1), UInt(1.W))))
  val flip   	 = Reg(Vec(NumWidth, Vec((NumWidth + 1), UInt(1.W))))
  val extra_flip = Reg(Vec(NumWidth, Vec((NumWidth + 1), UInt(1.W))))
  val flip_result = Reg(Vec(NumWidth, Vec((NumWidth + 1), UInt(1.W))))

  val x	      = Reg(UInt(NumWidth.W))
  val y	      = Reg(UInt(NumWidth.W))
  val t 	  = Reg(UInt(1.W))
  val in_length = Reg(UInt((log2_of_width + 1).W))
  val length_1 = Reg(UInt((log2_of_width + 1).W))
  val length_2 = Reg(UInt((log2_of_width + 1).W))
  val zones   = Reg(Vec((log2_of_width), Bool()))
  val boolt   = Reg(Bool())
  val shift   = Reg(Vec(NumWidth, UInt(dwordwidth.W)))
  val product = Reg(UInt(dwordwidth.W))
  val product_flip = Reg(Vec(dwordwidth, UInt(1.W)))
  //val result = Wire(Vec(dwordwidth, UInt(1.W)))
  
    
    when(io.loadingValues) {
    x := io.value1										//lv1
    y := io.value2
	t := io.signed
	in_length := io.sw_length
  }

	val vect = VecInit(t.asBools)
  	boolt := vect(0)									//lv2
	val boolt_1 = RegNext(boolt)						//lv3
	val boolt_2 = RegNext(boolt_1)						//lv4
	val boolt_3 = RegNext(boolt_2)						//lv5
	val boolt_4 = RegNext(boolt_3)						//lv6
	val boolt_5 = RegNext(boolt_4)						//lv7

	val length_bools = VecInit(in_length.asBools)
	length_1 := in_length								//lv2
	length_2 := length_1								//lv3
	val length_bools_2 = VecInit(length_2.asBools)		//lv3
	
	for (a <- 1 to log2_of_width)													//used a to avoid confusion with i in tabulate
	{
		val lesser_bits = VecInit.tabulate(a + 1)(i => length_bools(i))		//lv2
		zones(a - 1) := lesser_bits.reduce(_ || _)							//zones control signal, LSB is zone 2, MSB is zone 128
	}
	//printf("Print during simulation: zones is %b\n", zones.asUInt )

	val zones_1 = RegNext(zones)											//lv3
	val zones_2 = RegNext(zones_1)											//lv4
	val zones_3 = RegNext(zones_2)											//lv5
	val zones_4 = RegNext(zones_3)											//lv6
	val zones_5 = RegNext(zones_4)											//lv7

	for (i <- 0 until NumWidth)
	{
		for(j <- 0 until NumWidth)
		{																	//lv2
			multi(i)(j) := x(i) & y(j)										//stage 1: fill multi with sub results
		}
		//printf("Print during simulation: multi is %x\n", multi(i).asUInt )
	}

	for(i <- 0 until NumWidth)												//stage 2: invert edges of block if signed is true
	{
		for (j <- 0 until NumWidth)
		{
		for(k <- 1 until (log2_of_width - 1))	
		{
		val which_subword = 2 << k
		val next_subword = 4 << k
		val not_on_lateral_edge = (i + 1) % 4
		val not_on_vertical_edge = (j + 1) % 4   
		val on_lateral_edge = (i + 1) % which_subword
		val on_vertical_edge = (j + 1) % which_subword
		val on_next_lateral_edge = (i + 1) % next_subword
		val on_next_vertical_edge = (j + 1) % next_subword
		if(on_next_lateral_edge != 0 & on_next_vertical_edge != 0)			//must make sure not on other edges
		{
			if(on_lateral_edge == 0 & on_vertical_edge == 0)
			{sign_flip(i)(j) := multi(i)(j)}								//on corner: stays same
			else if(on_lateral_edge == 0 | on_vertical_edge == 0)
			{sign_flip(i)(j) := Mux(zones(k) && boolt, ~multi(i)(j), multi(i)(j))}
			else if(not_on_lateral_edge != 0 & not_on_vertical_edge != 0)	//lv3
			{sign_flip(i)(j) := multi(i)(j)}
		}
		else
		{
			if(on_next_lateral_edge == 0 & on_next_vertical_edge == 0)
			{sign_flip(i)(j) := multi(i)(j)}								//on corner: stays same
			else if(on_next_lateral_edge == 0 | on_next_vertical_edge == 0)
			{sign_flip(i)(j) := Mux(zones(k + 1) && boolt, ~multi(i)(j), multi(i)(j))}
		}
		}
		
			
		}
		//printf("Print during simulation: sign_flip is %x\n", sign_flip(i).asUInt )
		
	}

	for (i <- 0 until NumWidth)
	{
		for (j <- 0 until NumWidth)											//stage 3: set bits to zero if they are not in blocks
		{
		for (k <- 0 until (log2_of_width - 1))
		{
		val which_subword_1 = 2 << k
		val next_subword_1 = 4 << k
		val which_lateral_block = i / which_subword_1
		val which_vertical_block = j / which_subword_1
		val which_lateral_2_block = i / 2
		val which_vertical_2_block = j / 2
		val next_lateral_block = i / next_subword_1
		val next_vertical_block = j / next_subword_1
		
		if(which_vertical_2_block == which_lateral_2_block)
		{
			flip(i)(j) := sign_flip(i)(j)
		}
		else if(next_vertical_block == next_lateral_block)
		{
			if(which_lateral_block != which_vertical_block)
			{flip(i)(j) := sign_flip(i)(j) & (~zones_1(k))} 				//lv4
		}

		}
		}
		//printf("Print during simulation: flip is %x\n", flip(i).asUInt )
	}

	for (i <- 0 until NumWidth)												//stage 4: set start of block to flip if boolt
	{																		//and corresponding sw_in is true
		for(j <- 0 to NumWidth)
		{
			for (k <- 1 until (log2_of_width))
			{
			val which_subword_2 = 2 << k
			val extra_bit = i % which_subword_2
			if(extra_bit == 0 & j == i + which_subword_2)
			{
				extra_flip(i)(j) := length_bools_2(k + 1) & boolt_1			//lv4
			}
			}
		}
		
	}

	for (i <- 0 until NumWidth)												//not really sure if this is necessary,
	{																		//extra_flip only flips specific bits
		for (j <- 0 to NumWidth)											//rest is zero, but cant find better way to do this for now
		{
			when(extra_flip(i)(j) =/= 0.U)
			{
				flip_result(i)(j) := extra_flip(i)(j)
			}
			.otherwise
			{
				flip_result(i)(j) := flip(i)(j)								//lv5
			}
		}
		/*val flip_result_val = flip_result(i).asUInt
		printf("Print during simulation: flip is %b %b %b\n", (flip_result_val)(16), (flip_result_val)(15, 8), 
		(flip_result_val)(7, 0) )*/
	}
	

	for (i <- 0 until NumWidth)												//stage 5: left shift
	{
		shift(i) := flip_result(i).asUInt << i								//lv6
	}

	val result = VecInit.tabulate(NumWidth)(i => shift(i))
	product := result.reduce(_ + _)											//stage 6: accumulation
	//printf("Print during simulation: product is %x\n", product )			//lv7

	//printf("Print during simulation: zones is %b\n",zones.asUInt)
	//printf("Print during simulation: product is %x\n",product.asUInt)
	for (i <- 0 until dwordwidth)
	{
		for (k <- 1 until (log2_of_width))
		{
			val sub_result_width = 4 << k
			val next_sub_result_width = 8 << k								//stage 7: flip the bits of the result
			val flip_for_current = (i + 1) % sub_result_width
			val flip_for_next = (i + 1) % next_sub_result_width
			if((i + 1) % 8 != 0)
			{product_flip(i) := product(i)}  
			else if(flip_for_current == 0 & flip_for_next != 0)
			{product_flip(i) := Mux(zones_5(k) && boolt_5, ~product(i), product(i))}
			
		}
	}
	io.outputProduct := product_flip.asUInt
	//printf("Print during simulation: flipped product is %x\n",product_flip.asUInt )
}

import chisel3.stage.ChiselStage

object MultiplierDriver extends App {
	(new ChiselStage).emitVerilog(new mult_iter(16))
}