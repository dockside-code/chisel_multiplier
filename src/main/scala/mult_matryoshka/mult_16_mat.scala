// See README.md for license details.
package mult_16_mat
import chisel3._
import chisel3.util._



/**
  * Multiplier of combinational logic
  * supports 16 bit inputs
  * returns 32 bit results
  */
  
  
class mult_16_mat (NumWidth: Int) extends Module {
  val io = IO(new Bundle {
    val value1        = Input(UInt(NumWidth.W))
    val value2        = Input(UInt(NumWidth.W))
	val sw_32		  = Input(Bool())
	val sw_16		  = Input(Bool())
	val sw_8		  = Input(Bool())
	val sw_4		  = Input(Bool())
	val sw_2		  = Input(Bool())
	val signed		  = Input(UInt(1.W))
	val loadingValues = Input(Bool())
    val outputProduct = Output(UInt((NumWidth * 2).W))
  })
  val dwordwidth = NumWidth * 2
  val multi  	 = Reg(Vec(NumWidth, Vec(NumWidth, UInt(1.W))))
  val sign_flip  = Reg(Vec(NumWidth, Vec((NumWidth + 1), UInt(1.W))))
  val flip   	 = Reg(Vec(NumWidth, Vec((NumWidth + 1), UInt(1.W))))
  val extra_flip = Reg(Vec(NumWidth, Vec((NumWidth + 1), UInt(1.W))))

  val x	      = Reg(UInt(NumWidth.W))
  val y	      = Reg(UInt(NumWidth.W))
  val t 	  = Reg(UInt(1.W))
  val in32    = Reg(Bool())
  val in16    = Reg(Bool())
  val in8     = Reg(Bool())
  val in4     = Reg(Bool())
  val in2     = Reg(Bool())
  val z_16    = Reg(Bool())
  val z_8     = Reg(Bool())
  val z_4     = Reg(Bool())
  val z_2     = Reg(Bool())
  val boolt   = Reg(Bool())
  val shift   = Reg(Vec(NumWidth, UInt(dwordwidth.W)))
  val product = Reg(UInt(dwordwidth.W))
  val product_flip = Reg(Vec(dwordwidth, UInt(1.W)))
  //val result = Wire(Vec(dwordwidth, UInt(1.W)))
  
    
    when(io.loadingValues) {
    x := io.value1
    y := io.value2
	t := io.signed
	in32 := io.sw_32
	in16 := io.sw_16
	in8  := io.sw_8
	in4  := io.sw_4									//lv1
	in2  := io.sw_2
  }

	val vect = VecInit(t.asBools)
  	boolt := vect(0)								//lv2
	val boolt_1 = RegNext(boolt)						//lv3
	val boolt_2 = RegNext(boolt_1)						//lv4
	val boolt_3 = RegNext(boolt_2)						//lv5
	val boolt_4 = RegNext(boolt_3)						//lv6
	val boolt_5 = RegNext(boolt_4)						//lv7
	
	z_16 := ~((in2 || in4) || (in16 || in8))		//lv2
	z_8 := ~((in2 || in4) || in8)
	z_4 := ~(in2 || in4)
	z_2 := ~in2

	val z_16_1 = RegNext(z_16)							//lv3
	val z_8_1  = RegNext(z_8)							
	val z_4_1  = RegNext(z_4)
	val z_2_1  = RegNext(z_2)

	val z_16_2 = RegNext(z_16_1)							//lv4
	val z_8_2  = RegNext(z_8_1)							
	val z_4_2  = RegNext(z_4_1)
	val z_2_2  = RegNext(z_2_1)

	val z_16_3 = RegNext(z_16_2)							//lv5
	val z_8_3  = RegNext(z_8_2)							
	val z_4_3  = RegNext(z_4_2)
	val z_2_3  = RegNext(z_2_2)

	val z_16_4 = RegNext(z_16_3)							//lv6
	val z_8_4  = RegNext(z_8_3)							
	val z_4_4  = RegNext(z_4_3)
	val z_2_4  = RegNext(z_2_3)

	val z_16_5 = RegNext(z_16_4)							//lv6
	val z_8_5  = RegNext(z_8_4)							
	val z_4_5  = RegNext(z_4_4)
	val z_2_5  = RegNext(z_2_4)

	val in32_1 = RegNext(in32)							//lv2
	val in32_2 = RegNext(in32_1)						//lv3
	val in32_3 = RegNext(in32_2)						//lv4

	val in16_1 = RegNext(in16)							//lv2
	val in16_2 = RegNext(in16_1)						//lv3
	val in16_3 = RegNext(in16_2)						//lv4

	val in8_1 = RegNext(in8)
	val in8_2 = RegNext(in8_1)
	val in8_3 = RegNext(in8_2)

	val in4_1 = RegNext(in4)
	val in4_2 = RegNext(in4_1)
	val in4_3 = RegNext(in4_2)

	val in2_1 = RegNext(in2)
	val in2_2 = RegNext(in2_1)
	val in2_3 = RegNext(in2_2)

	for (i <- 0 until NumWidth)
	{
		for (j <- 0 until NumWidth)
		{
		
		multi(i)(j) := x(i) & y(j)					//lv2

		val which_vertical_2_block = i / 2
		val which_vertical_4_block = i / 4
		val which_vertical_8_block = i / 8
		val which_vertical_16_block = i / 16

		val which_lateral_2_block = j / 2
		val which_lateral_4_block = j / 4
		val which_lateral_8_block = j / 8
		val which_lateral_16_block = j / 16

		val on_vertical_2_edge  = (j + 2) % 2
		val on_vertical_4_edge  = (j + 2) % 4
		val on_vertical_8_edge  = (j + 2) % 8
		val on_vertical_16_edge = (j + 2) % 16
		val on_vertical_32_edge = (j + 2) % 32

		val on_lateral_2_edge  = (i + 2) % 2
		val on_lateral_4_edge  = (i + 2) % 4
		val on_lateral_8_edge  = (i + 2) % 8
		val on_lateral_16_edge = (i + 2) % 16
		val on_lateral_32_edge = (i + 2) % 32

		if(on_vertical_32_edge == 1 & on_lateral_32_edge == 1)
		{
			sign_flip(i)(j) := multi(i)(j) 
		}
		else if(on_vertical_32_edge == 1 | on_lateral_32_edge == 1)
		{																				
			sign_flip(i)(j) := Mux(boolt, ~multi(i)(j), multi(i)(j))			
		}
		else if(on_vertical_16_edge == 1 & on_lateral_16_edge == 1)
		{
			sign_flip(i)(j) := multi(i)(j) 
		}
		else if(on_vertical_16_edge == 1 | on_lateral_16_edge == 1)
		{																				
			sign_flip(i)(j) := Mux((~z_16) && boolt, ~multi(i)(j), multi(i)(j))			
		}
		else if(on_vertical_8_edge == 1 & on_lateral_8_edge == 1)
		{
			sign_flip(i)(j) := multi(i)(j)
		}
		else if(on_vertical_8_edge == 1 | on_lateral_8_edge == 1)
		{
			sign_flip(i)(j) := Mux((~z_8) && boolt, ~multi(i)(j), multi(i)(j))
		}
		else if(on_vertical_4_edge == 1 & on_lateral_4_edge == 1)
		{
			sign_flip(i)(j) := multi(i)(j)
		}
		else if(on_vertical_4_edge == 1 | on_lateral_4_edge == 1)
		{
			sign_flip(i)(j) := Mux((~z_4) && boolt, ~multi(i)(j), multi(i)(j))
		}
		else
		{
			sign_flip(i)(j) := multi(i)(j)												//lv3
		}

		if(which_lateral_16_block == which_vertical_16_block)
		{
			if(which_lateral_8_block == which_vertical_8_block)
			{
				if(which_lateral_4_block == which_vertical_4_block)
				{
					if(which_lateral_2_block == which_vertical_2_block)
					{flip(i)(j) := sign_flip(i)(j)}										//lv4
					else
					{flip(i)(j) := sign_flip(i)(j) & z_2_1}
				}
				else
				{flip(i)(j) := sign_flip(i)(j) & z_4_1}
			}
			else
			{flip(i)(j) := sign_flip(i)(j) & z_8_1}


		}
		else
		{flip(i)(j) := sign_flip(i)(j) & z_16_1}
		}
		//val flappy_sign = sign_flip(i).asUInt
		//printf("Print during simulation: sign_flip is %x\n", flappy_sign )
	}

	for (i <- 0 until NumWidth)
	{
		for(j <- 0 to NumWidth)
		{

		val extra_bit_2  = j % 2
		val extra_bit_4  = j % 4
		val extra_bit_8  = j % 8
		val extra_bit_16 = j % 16
		val extra_bit_32 = j % 32

		if(extra_bit_32 == 0 & j == i + 32)							
		{																		//lv5
		extra_flip(i)(j) := Mux(in32_3, boolt_2, flip(i)(j))					
		}
		else if(extra_bit_16 == 0 & j == i + 16)							
		{																		//lv5
		extra_flip(i)(j) := Mux(in16_3, boolt_2, flip(i)(j))					
		}
		else if(extra_bit_8 == 0 & j == i + 8)
		{
		extra_flip(i)(j) := Mux(in8_3, boolt_2, flip(i)(j))
		}
		else if(extra_bit_4 == 0 & j == i + 4)
		{
		extra_flip(i)(j) := Mux(in4_3, boolt_2, flip(i)(j))
		}
		else
		{extra_flip(i)(j) := flip(i)(j)}
		}

		

		//val flappy = extra_flip(i).asUInt
		//printf("Print during simulation: flip is %x\n", flappy )
		shift(i) := extra_flip(i).asUInt << i									//lv6
		
	}


	val result = VecInit.tabulate(NumWidth)(i => shift(i))
	product := result.reduce(_ + _)
	//printf("Print during simulation: product is %x\n", product )												//lv7
	for (i <- 0 until dwordwidth)
	{
		val bit_2_flip  = (i + 1) % 4
		val bit_4_flip  = (i + 1) % 8
		val bit_8_flip  = (i + 1) % 16
		val bit_16_flip = (i + 1) % 32
		val bit_32_flip = (i + 1) % 64
		if (bit_32_flip == 0)
		{product_flip(i) := Mux(boolt_5, ~product(i), product(i))}
		else if (bit_16_flip == 0)
		{product_flip(i) := Mux((~z_16_5) && boolt_5, ~product(i), product(i))}				
		else if (bit_8_flip == 0)
		{product_flip(i) := Mux((~z_8_5) && boolt_5, ~product(i), product(i))}				
		else if (bit_4_flip == 0)
		{product_flip(i) := Mux((~z_4_5) && boolt_5, ~product(i), product(i))}
		else
		{product_flip(i) := product(i)}
		
	}

	io.outputProduct := product_flip.asUInt
	//printf("Print during simulation: flipped product is %x\n", product_flip.asUInt )
}

